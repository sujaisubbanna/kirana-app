import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/models/shared/item_model.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/shared/store_tags_enum.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/models/store/payment_details_type_enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';
import 'package:skipyq_app/services/auth/auth_service.dart';

class OwnerService {
  final Dio _dio = Dio();
  final String url = getEndpoint();
  final AuthService authService = AuthService();

  Future<void> addStore(
      {String name,
      LocationModel location,
      String contactNo,
      String image,
      StoreType type,
      List<StoreTag> tags}) async {
    final token = Get.find<AppController>().authToken;
    await _dio.post(
      '$url/store',
      data: {
        'name': name,
        'image': image,
        'contactNo': contactNo,
        'location': location.toJson(),
        'type': EnumToString.convertToString(type),
        'tags': tags.map((e) => EnumToString.convertToString(e)).toList(),
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
  }

  Future<void> updateStore({
    String id,
    String name,
    LocationModel location,
    String contactNo,
    String image,
    StoreType type,
    List<StoreTag> tags,
  }) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/store/update',
      data: {
        'id': id,
        'name': name,
        'contactNo': contactNo,
        'location': location.toJson(),
        'image': image,
        'type': EnumToString.convertToString(type),
        'tags': tags.map((e) => EnumToString.convertToString(e)).toList(),
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    Store temp = Store.fromJson(response.data);
    Get.find<OwnerController>().setStore(id, temp);
  }

  Future<void> toggleOpenStatus(String id) async {
    final token = Get.find<AppController>().authToken;
    final store =
        Get.find<OwnerController>().stores.firstWhere((x) => x.id == id);
    await _dio.post(
      '$url/store/openStatus/${store.id}',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    store.open = !store.open;
    Get.find<OwnerController>().setStore(id, store);
  }

  Future<void> toggleOnlinePaymentEnabled(String id) async {
    final token = Get.find<AppController>().authToken;
    final store =
        Get.find<OwnerController>().stores.firstWhere((x) => x.id == id);
    await _dio.post(
      '$url/store/onlinePayment/${store.id}',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    store.onlinePaymentEnabled = !store.onlinePaymentEnabled;
    Get.find<OwnerController>().setStore(id, store);
  }

  Future<void> togglePaymentRequired(String id) async {
    final token = Get.find<AppController>().authToken;
    final store =
        Get.find<OwnerController>().stores.firstWhere((x) => x.id == id);
    await _dio.post(
      '$url/store/paymentRequired/${store.id}',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    store.paymentRequired = !store.paymentRequired;
    if (store.paymentRequired) {
      store.onlinePaymentEnabled = true;
    }
    Get.find<OwnerController>().setStore(id, store);
  }

  Future<void> deletePaymentDetails(
      String id, PaymentDetailsTypeEnum type) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/store/deletePaymentDetails/$id/${EnumToString.convertToString(type)}',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    Store temp = Store.fromJson(response.data);
    Get.find<OwnerController>().setStore(id, temp);
  }

  Future<void> addPaymentDetails(
    String id,
    PaymentDetails paymentDetails,
    PaymentDetailsTypeEnum type,
  ) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/store/addPaymentDetails/$id/${EnumToString.convertToString(type)}',
      data: paymentDetails.toJson(),
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    Store temp = Store.fromJson(response.data);
    Get.find<OwnerController>().setStore(id, temp);
  }

  Future<void> getStores(String cursor, int limit) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/store/owner',
      queryParameters: {
        'cursor': cursor,
        'limit': limit,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final List<Store> stores = (response.data as List)
            .map((e) => Store.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [];
    Get.find<OwnerController>().setStores(stores);
  }

  Future<List<Order>> getStoreOrders(
      String storeId, OrderType orderType) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/order/store/$storeId',
      queryParameters: {
        'orderType': EnumToString.convertToString(orderType),
        'cursor': null,
        'limit': 100,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final List<Order> orders =
        (response.data as List).map((e) => Order.fromJson(e)).toList() ?? [];
    return orders;
  }

  Future<Order> getOrder(String orderId, OrderType orderType) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/order/owner/store/$orderId',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<OwnerController>().setOrder(order, orderType);
    return order;
  }

  Future<Order> cancelOrder(String orderId) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/order/cancel/$orderId',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<OwnerController>()
        .removeOrder(order, OrderType.OPEN, OrderType.CANCELLED);
    await authService.whoAmI(token);
    return order;
  }

  Future<Order> completeOrder(String orderId) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/order/complete/$orderId',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<OwnerController>().removeOrder(
      order,
      OrderType.OPEN,
      OrderType.COMPLETED,
    );
    await authService.whoAmI(token);
    return order;
  }

  Future<Order> acceptOrder(
      String orderId, List<Item> items, double discount) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/order/accept/$orderId',
      data: {'items': items, 'discount': discount},
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<OwnerController>().setOrder(order, OrderType.OPEN);
    return order;
  }

  Future<Order> setOrderPacked(
      String orderId, List<Item> items, double discount) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/order/status/$orderId',
      data: {'items': items, 'discount': discount},
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<OwnerController>().setOrder(order, OrderType.OPEN);
    await authService.whoAmI(token);
    return order;
  }
}
