import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/controllers/shared/admin_controller.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/models/admin/stats_model.dart';
import 'package:skipyq_app/models/owner/owner_model.dart';
import 'package:skipyq_app/models/shared/time_period_enum.dart';

class AdminService {
  final Dio _dio = Dio();
  final String url = getEndpoint();

  Future<Stats> getStats(TimePeriod timePeriod) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/admin/stats',
      queryParameters: {
        'timePeriod': EnumToString.convertToString(timePeriod),
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Stats stats = Stats.fromJson(response.data);
    return stats;
  }

  Future<void> createOwner({String name, String mobile}) async {
    final token = Get.find<AppController>().authToken;
    await _dio.post(
      '$url/owner',
      data: {
        'name': name,
        'mobile': mobile,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
  }

  Future<List<Owner>> getOwners() async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/owner',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final List<Owner> owners = (response.data as List)
            .map((e) => Owner.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [];
    Get.find<AdminController>().setOwners(owners);
    return owners;
  }
}
