import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/config/firebase_notification.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/models/customer/customer_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';

class AuthService {
  final Dio _dio = Dio();
  final String url = getEndpoint();

  Future<void> sendConfirmationCode({
    String mobile,
  }) async {
    final endpoint = sendCodeEndpoint(url);
    await _dio.get(endpoint, queryParameters: {'mobile': mobile});
  }

  Future<Map<String, dynamic>> verifyConfirmationCode({
    String confirmationCode,
    String mobile,
  }) async {
    final endpoint = verifyCodeEndpoint(url);
    final response = await _dio.post(endpoint, data: {
      'mobile': mobile,
      'otp': confirmationCode,
    });
    final token = response.data['token'] as String;
    Get.find<AppController>().setAuthToken(token);
    return {
      'token': token,
      'data': response.data['data'],
    };
  }

  Future<void> resendConfirmationCode({String mobile}) async {
    await _dio.get('$url/auth/resend', queryParameters: {'mobile': mobile});
  }

  Future<Map<String, dynamic>> whoAmI(String token) async {
    final response = await _dio.get(
      '$url/auth/whoami',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    return setUser(response.data, token);
  }

  Future<void> ownerSignUp(String name, String mobile) async {
    await _dio.post(
      DISCORD_WEBHOOK_URL,
      data: {
        'content': 'OWNER SIGN UP $name $mobile',
      },
    );
  }

  // ignore: missing_return
  Map<String, dynamic> setUser(
    Map<String, dynamic> data,
    String token,
  ) {
    FirebaseNotifications().getToken(
      existingToken: data['identity']['pushToken'],
    );
    final Map<String, dynamic> payload = Jwt.parseJwt(token);
    final role =
        EnumToString.fromString(Roles.values, payload['role'] as String);
    Get.find<UserController>().setUser(data, role);
    switch (role) {
      case Roles.ADMIN:
        return {
          'route': routes.DASHBOARD,
          'data': null,
        };
      case Roles.OWNER:
        return {
          'route': routes.STORES,
          'data': null,
        };
        break;
      case Roles.CUSTOMER:
        final Customer customer = Customer.fromJson(data);
        if (customer.name == null) {
          return {
            'route': routes.CUSTOMER_SIGN_UP,
            'data': null,
          };
        } else {
          return {
            'route': routes.HOME,
            'data': null,
          };
        }
    }
  }

  void updatePushToken() async {
    final token = Get.find<AppController>().authToken;
    final pushToken = Get.find<AppController>().pushToken;
    if (pushToken != null) {
      await _dio.post(
        '$url/auth/pushToken',
        data: {
          'token': pushToken,
        },
        options: Options(
          headers: {'Authorization': 'Bearer $token'},
        ),
      );
    }
  }
}
