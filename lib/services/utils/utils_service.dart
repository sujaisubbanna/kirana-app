import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/models/shared/order_status_enum.dart';
import 'package:skipyq_app/models/shared/store_tags_enum.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class UtilsService {
  Dio dio = Dio();
  final String url = getEndpoint();

  Future<String> uploadImage(String base64) async {
    final String authToken = Get.find<AppController>().authToken;
    final response = await dio.post(
      '$url/store/image-upload',
      options: Options(
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $authToken',
        },
        responseType: ResponseType.plain,
      ),
      data: {
        'image': base64,
      },
    );
    if (response.data != null) {
      return response.data as String;
    } else {
      throw response.statusMessage;
    }
  }

  // ignore: missing_return
  Widget getStoreIcon(StoreType type) {
    switch (type) {
      case StoreType.GENERAL:
        return SvgContainer(
          height: 1.w(),
          width: 1.w(),
          path: 'assets/images/icons/general_store.svg',
        );
      case StoreType.MEDICAL:
        return SvgContainer(
          height: 1.w(),
          width: 1.w(),
          path: 'assets/images/icons/medical_store.svg',
        );
      case StoreType.RESTAURANT:
        return SvgContainer(
          height: 1.w(),
          width: 1.w(),
          path: 'assets/images/icons/restaurant_store.svg',
        );
    }
  }

  // ignore: missing_return
  String getStoreHint(StoreType type) {
    switch (type) {
      case StoreType.GENERAL:
        return 'Maggi 10Rs packet, Atta 100g';
      case StoreType.MEDICAL:
        return 'Crocin Tablet, Anacin Strip';
      case StoreType.RESTAURANT:
        return 'Biryani, Masala Dosa';
    }
  }

  // ignore: missing_return
  String getStoreNotes(StoreType type) {
    switch (type) {
      case StoreType.GENERAL:
        return 'Carry bag, any other instruction';
      case StoreType.MEDICAL:
        return 'Patient name, Doctor name';
      case StoreType.RESTAURANT:
        return 'Don\'t send cutlery, Less spicy';
    }
  }

  List<StoreTag> getStoreTags(StoreType type) {
    switch (type) {
      case StoreType.MEDICAL:
        return [StoreTag.DRUG_STORE, StoreTag.OPTICAL];
      case StoreType.RESTAURANT:
        return [
          StoreTag.BAKERY,
          StoreTag.CHAATS,
          StoreTag.CHINESE,
          StoreTag.FAST_FOOD,
          StoreTag.JUICE,
          StoreTag.NORTH_INDIAN,
          StoreTag.SOUTH_INDIAN
        ];
      case StoreType.GENERAL:
      default:
        return [
          StoreTag.CONDIMENTS,
          StoreTag.DRY_FRUITS,
          StoreTag.PROVISION,
          StoreTag.VEGETABLE
        ];
    }
  }

  // ignore: missing_return
  Color getOrderStatusColor(OrderStatus status) {
    switch (status) {
      case OrderStatus.CREATED:
        return Colors.blue;
      case OrderStatus.AWAITING_PAYMENT:
        return Color(0xFFf9A825);
      case OrderStatus.ACCEPTED:
        return Colors.green;
      case OrderStatus.PACKED:
        return Color(0xFFEB790D);
      case OrderStatus.CANCELLED:
        return Color(0xFFD64553);
      case OrderStatus.COMPLETED:
        return Color(0xFF0281D3);
    }
  }
}
