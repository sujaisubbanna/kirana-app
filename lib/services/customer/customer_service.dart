import 'package:dio/dio.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/controllers/shared/customer_controller.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/identity_model.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';

class CustomerService {
  Dio _dio = Dio();
  final String url = getEndpoint();

  Future<void> signUp(String name, LocationModel address) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/customer/sign-up',
      data: {
        'name': name,
        'location': address.toJson(),
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    Get.find<UserController>().setUser(response.data, Roles.CUSTOMER);
  }

  Future<void> feedback(String feedback, Roles role) async {
    final token = Get.find<AppController>().authToken;
    var mobile;
    if (role == Roles.CUSTOMER) {
      mobile =
          (Get.find<UserController>().customer.identity as Identity).mobile;
    } else if (role == Roles.OWNER) {
      mobile = (Get.find<UserController>().owner.identity as Identity).mobile;
    }
    print('$url/customer/feedback');
    await _dio.post(
      '$url/customer/feedback',
      data: {
        'body': '${EnumToString.convertToString(role)} - $mobile - $feedback'
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
  }

  Future<void> update(String name, LocationModel address) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/customer/update',
      data: {
        'name': name,
        'location': address.toJson(),
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    Get.find<UserController>().setUser(response.data, Roles.CUSTOMER);
  }

  Future<void> getStores() async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/store/customer',
      queryParameters: {
        'cursor': null,
        'limit': 100,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final List<Store> temp = (response.data as List)
            .map((e) => Store.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [];
    Get.find<CustomerController>().setStores(temp);
  }

  Future<Order> createOrder(
    String storeId,
    List<Map<String, dynamic>> items,
    String notes,
  ) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.post(
      '$url/order',
      data: {
        'items': items,
        'store': storeId,
        'notes': notes,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<CustomerController>().addOrder(order, OrderType.OPEN);
    return order;
  }

  Future<void> getOrders(OrderType orderType) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      orderType == OrderType.RECENT
          ? '$url/order/customer/recent'
          : '$url/order/customer',
      queryParameters: {
        'orderType': orderType == OrderType.RECENT
            ? null
            : EnumToString.convertToString(orderType),
        'cursor': null,
        'limit': orderType == OrderType.RECENT ? 4 : 100,
      },
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final List<Order> orders = (response.data as List)
            .map(
              (e) => Order.fromJson(e),
            )
            .toList() ??
        [];
    if (orderType != OrderType.RECENT) {
      Get.find<UserController>().setOrders(orders, orderType);
    }
    Get.find<CustomerController>().setOrders(orders, orderType);
  }

  Future<Order> getOrder(String orderId, OrderType orderType) async {
    final token = Get.find<AppController>().authToken;
    final response = await _dio.get(
      '$url/order/customer/$orderId',
      options: Options(
        headers: {'Authorization': 'Bearer $token'},
      ),
    );
    final Order order = Order.fromJson(response.data);
    Get.find<CustomerController>().setOrder(order, orderType);
    return order;
  }
}
