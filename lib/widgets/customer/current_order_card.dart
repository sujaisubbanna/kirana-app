import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';

class CurrentOrderCard extends StatelessWidget {
  const CurrentOrderCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      child: GetBuilder<UserController>(
        builder: (value) {
          if (value
              .customer
              .orders[EnumToString.convertToString(OrderType.OPEN)]
              .isNotEmpty) {
            return Container(
              color: accentColor,
              height: 7.h(),
              width: 100.w(),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AutoSizedText(
                      text: 'You have open orders',
                      height: 1.h(),
                      width: 10.w(),
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Get.toNamed(routes.CUSTOMER_ORDERS);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(
                            20,
                          ),
                        ),
                        height: 5.h(),
                        width: 20.w(),
                        child: AutoSizedText(
                          text: 'VIEW',
                          align: Alignment.center,
                          height: 4.h(),
                          width: 10.w(),
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Container(
              child: null,
            );
          }
        },
      ),
    );
  }
}
