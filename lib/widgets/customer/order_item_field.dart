import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class OrderItemField extends StatefulWidget {
  final int idx;
  final GlobalKey<FormBuilderState> formKey;
  final StoreType storeType;
  final UtilsService utilsService = UtilsService();

  OrderItemField({
    Key key,
    @required this.idx,
    @required this.formKey,
    @required this.storeType,
  }) : super(key: key);

  @override
  _OrderItemFieldState createState() => _OrderItemFieldState();
}

class _OrderItemFieldState extends State<OrderItemField> {
  int count = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          AutoSizedText(
            text: '${widget.idx + 1}.',
            width: 1.w(),
            height: 0.5.h(),
            minFontSize: 18,
            maxFontSize: 20,
          ),
          CustomInput(
            minLength: 3,
            maxLength: 100,
            name: 'name_${widget.idx}',
            hint: widget.utilsService.getStoreHint(widget.storeType),
            requiredErrorText: 'Please enter item name',
            width: 60.w(),
          ),
          CustomInput(
            name: 'qty_${widget.idx}',
            requiredErrorText: 'Quantity',
            width: 20.w(),
            initialValue: '1',
          ),
        ],
      ),
    );
  }
}
