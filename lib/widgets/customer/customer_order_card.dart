import 'package:flutter/material.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/order/order_status.dart';

class CustomerOrderCard extends StatelessWidget {
  final Order order;
  final Function onTap;

  CustomerOrderCard({
    @required this.order,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 3.h(),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 3,
        horizontal: 6.w(),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizedText(
                text: (order.store as Store).name,
                height: 0.75.h(),
                width: 12.w(),
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 20,
                ),
                multilineEnable: true,
                align: Alignment.centerLeft,
                textAlign: TextAlign.left,
              ),
              AutoSizedText(
                text: '#${order.orderNo.toString()}',
                height: 0.5.h(),
                width: 12.w(),
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
                align: Alignment.centerLeft,
              ),
              OrderStatusChip(
                status: order.status,
              ),
            ],
          ),
          CustomButton(
            height: 5.h(),
            width: 20.w(),
            text: 'VIEW',
            solid: false,
            onTap: onTap,
            color: accentColor,
          ),
        ],
      ),
    );
  }
}
