import 'package:auto_size_text/auto_size_text.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';
import 'package:recase/recase.dart';

class CustomerStoreCard extends StatelessWidget {
  CustomerStoreCard({
    Key key,
    @required this.store,
    @required this.titleGroup,
  }) : super(key: key);

  final UtilsService utilsService = UtilsService();
  final Store store;
  final AutoSizeGroup titleGroup;

  void createOrder() {
    Get.toNamed(routes.CREATE_ORDER, arguments: store);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: createOrder,
      child: Card(
        elevation: 0,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 4,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              store.image == null
                  ? SvgContainer(
                      height: 4.w(),
                      width: 4.w(),
                      path: 'assets/images/icons/stores.svg',
                      color: accentColor,
                    )
                  : Container(
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.25),
                            spreadRadius: 0,
                            blurRadius: 4,
                            offset: const Offset(0, 4),
                          ),
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image.network(
                          store.image,
                          height: 25.w(),
                          width: 25.w(),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
              Container(
                width: 70.w(),
                padding: EdgeInsets.symmetric(
                  vertical: 0.5.h(),
                  horizontal: 2.w(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizedText(
                      group: titleGroup,
                      align: Alignment.centerLeft,
                      text: store.name,
                      height: 0.8.h(),
                      width: 20.w(),
                      minFontSize: 18,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: store.tags
                            .map(
                              (e) => Container(
                                margin: EdgeInsets.symmetric(
                                  horizontal: 1.w(),
                                ),
                                child: Chip(
                                  backgroundColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: accentColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                  ),
                                  labelPadding: EdgeInsets.symmetric(
                                    horizontal: 4,
                                    vertical: 0,
                                  ),
                                  label: Text(
                                    EnumToString.convertToString(e).titleCase,
                                  ),
                                  shape: StadiumBorder(
                                    side: BorderSide(
                                      color: accentColor,
                                      width: 1,
                                    ),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            utilsService.getStoreIcon(
                              store.type,
                            ),
                            SizedBox(
                              width: 1.w(),
                            ),
                            AutoSizedText(
                              text: '${EnumToString.convertToString(
                                store.type,
                              )}',
                              width: 7.w(),
                              height: 0.5.h(),
                            )
                          ],
                        ),
                        AutoSizedText(
                          align: Alignment.centerLeft,
                          text: '${store.dist} Km',
                          height: 0.5.h(),
                          width: 3.w(),
                          style: TextStyle(
                            fontWeight: FontWeight.w800,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
