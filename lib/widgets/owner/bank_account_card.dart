import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';

class BankAccountCard extends StatelessWidget {
  final BankAccount bankAccount;
  final Function deleteAccount;

  const BankAccountCard({
    Key key,
    @required this.bankAccount,
    @required this.deleteAccount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80.w(),
      padding: EdgeInsets.all(3.w()),
      margin: EdgeInsets.all(3.w()),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
          2.w(),
        ),
        boxShadow: [
          BoxShadow(
            color: lightGreyColor.withOpacity(0.5),
            spreadRadius: 0.2,
            blurRadius: 1,
            offset: const Offset(1, 1),
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Bank Account',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              IconButton(
                onPressed: () {
                  Get.dialog(
                    CustomAlertDialog(
                      title: 'Do you want to delete the account',
                      onReturn: () => Get.back(),
                      onConfirm: () {
                        Get.back();
                        deleteAccount();
                      },
                      returnTitle: 'Cancel',
                      confirmTitle: 'Delete',
                    ),
                  );
                },
                icon: const Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 2.h(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Name:',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                bankAccount.name,
                textAlign: TextAlign.left,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'IFSC:',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                bankAccount.ifsc,
                textAlign: TextAlign.left,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Account number:',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                bankAccount.accountNumber,
                textAlign: TextAlign.left,
              )
            ],
          ),
        ],
      ),
    );
  }
}
