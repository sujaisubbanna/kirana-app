import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:skipyq_app/models/shared/item_model.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class OrderItemEditField extends StatefulWidget {
  final int idx;
  final GlobalKey<FormBuilderState> formKey;
  final Item item;

  const OrderItemEditField({
    Key key,
    @required this.idx,
    @required this.formKey,
    @required this.item,
  }) : super(key: key);

  @override
  _OrderItemEditFieldState createState() => _OrderItemEditFieldState();
}

class _OrderItemEditFieldState extends State<OrderItemEditField>
    with AfterLayoutMixin<OrderItemEditField> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    widget.formKey.currentState
        .setInternalFieldValue('qty_${widget.idx}', widget.item.qty);
    widget.formKey.currentState
        .setInternalFieldValue('name_${widget.idx}', widget.item.name);
    widget.formKey.currentState
        .setInternalFieldValue('price_${widget.idx}', widget.item.price);
  }

  int count = 1;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Visibility(
            visible: false,
            child: CustomInput(
              minLength: 3,
              labelText: 'Name',
              maxLength: 100,
              name: 'name_${widget.idx}',
              hint: 'Rice flour 100g, Maggi',
              requiredErrorText: 'Please enter item name',
              width: 30.w(),
              initialValue: widget.item.name,
            ),
          ),
          AutoSizedText(
            text: widget.item.name,
            height: 1.h(),
            width: 8.w(),
            style: TextStyle(
              fontSize: 16,
            ),
            align: Alignment.centerLeft,
            multilineEnable: true,
          ),
          CustomInput(
            labelText: 'Qty',
            name: 'qty_${widget.idx}',
            requiredErrorText: 'Please enter item quantity',
            width: 18.w(),
            initialValue: widget.item.qty,
          ),
          CustomInput(
            labelText: 'Total Price',
            name: 'price_${widget.idx}',
            width: 25.w(),
            initialValue: '0',
            keyboardType: TextInputType.number,
          ),
        ],
      ),
    );
  }
}
