import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/config/size_config_extension.dart';

class OwnerDrawer extends StatelessWidget {
  final env = dotenv.env['ENV'];

  OwnerDrawer({
    Key key,
  }) : super(key: key);

  void logout() {
    Get.find<AppController>().logout();
    Get.offAndToNamed(routes.LOGIN);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: accentColor,
            ),
            child: GetBuilder<UserController>(
              builder: (value) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    AutoSizedText(
                      text: value.owner.name,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 24,
                      ),
                      height: 0.5.h(),
                      width: 10.w(),
                    ),
                  ],
                );
              },
            ),
          ),
          ListTile(
            title: AutoSizedText(
              text: 'Feedback',
              height: 1.h(),
              width: 2.w(),
              align: Alignment.centerLeft,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            leading: Icon(
              Icons.feedback_outlined,
              color: primaryColor,
            ),
            onTap: () {
              Navigator.pop(context);
              Get.toNamed(routes.FEEDBACK, arguments: Roles.OWNER);
            },
          ),
          ListTile(
            title: AutoSizedText(
              text: 'Share',
              height: 1.h(),
              width: 2.w(),
              align: Alignment.centerLeft,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            leading: Icon(
              Icons.share,
              color: primaryColor,
            ),
            onTap: () {
              Navigator.pop(context);
              Share.share(
                  'SkipyQ - Pick up from your neighborhood stores! Download our app here https://skipyq.page.link/qL6j');
            },
          ),
          env != 'prod'
              ? ListTile(
                  title: AutoSizedText(
                    text: 'Logout',
                    height: 1.h(),
                    width: 2.w(),
                    align: Alignment.centerLeft,
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  leading: Icon(
                    Icons.logout,
                    color: primaryColor,
                  ),
                  onTap: () {
                    logout();
                  },
                )
              : Container(
                  child: null,
                ),
        ],
      ),
    );
  }
}
