import 'package:flutter/material.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/customer/customer_model.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/order/order_status.dart';

class StoreOrderCard extends StatelessWidget {
  final Order store;
  final Function onTap;

  StoreOrderCard({
    @required this.store,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 3.h(),
      ),
      padding: EdgeInsets.symmetric(
        vertical: 3,
        horizontal: 6.w(),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AutoSizedText(
                text: (store.customer as Customer).name,
                height: 0.5.h(),
                width: 12.w(),
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 18,
                ),
                align: Alignment.centerLeft,
              ),
              AutoSizedText(
                text: '#${store.orderNo.toString()}',
                height: 0.5.h(),
                width: 12.w(),
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
                align: Alignment.centerLeft,
              ),
              OrderStatusChip(
                status: store.status,
              ),
            ],
          ),
          CustomButton(
            height: 5.h(),
            width: 20.w(),
            text: 'VIEW',
            solid: false,
            onTap: onTap,
            color: accentColor,
          ),
        ],
      ),
    );
  }
}
