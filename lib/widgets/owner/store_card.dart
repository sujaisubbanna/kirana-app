import 'package:badges/badges.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';

class StoreCard extends StatelessWidget {
  StoreCard({
    @required this.store,
  });
  final Store store;

  void manageStore(Store store) {
    Get.toNamed(routes.MANAGE_STORE, arguments: store.id);
  }

  void viewOrders() {
    Get.toNamed(
      routes.STORE_ORDERS,
      arguments: store,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 1,
      ),
      height: 27.h(),
      child: Row(
        children: [
          store.image == null
              ? SvgContainer(
                  height: 6.w(),
                  width: 6.w(),
                  path: 'assets/images/icons/stores.svg',
                  color: accentColor,
                )
              : Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.25),
                        spreadRadius: 0,
                        blurRadius: 4,
                        offset: const Offset(0, 4),
                      ),
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.network(
                      store.image,
                      height: 25.w(),
                      width: 25.w(),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
          SizedBox(
            width: 5.w(),
          ),
          Container(
            width: 60.w(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizedText(
                  align: Alignment.centerLeft,
                  text: store.name,
                  height: 1.h(),
                  width: 30.w(),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 24,
                  ),
                  multilineEnable: true,
                ),
                SizedBox(
                  height: 1.h(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomButton(
                      height: 5.h(),
                      width: 18.w(),
                      text: 'EDIT',
                      icon: null,
                      onTap: () {
                        manageStore(store);
                      },
                      solid: false,
                      color: accentColor,
                    ),
                    CustomButton(
                      height: 5.h(),
                      width: 35.w(),
                      text: 'VIEW ORDERS',
                      icon: store
                              .orders[
                                  EnumToString.convertToString(OrderType.OPEN)]
                              .isNotEmpty
                          ? Badge(
                              badgeColor: Colors.blue,
                              borderRadius: BorderRadius.circular(4),
                              badgeContent: Text(
                                store
                                    .orders[EnumToString.convertToString(
                                        OrderType.OPEN)]
                                    .length
                                    .toString(),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            )
                          : null,
                      onTap: viewOrders,
                      solid: false,
                      color: accentColor,
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
