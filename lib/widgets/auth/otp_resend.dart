import 'dart:async';

import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/services/auth/auth_service.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:sms_autofill/sms_autofill.dart';

class OTPResend extends StatefulWidget {
  final String mobile;
  final Function resetOTP;

  const OTPResend({
    this.mobile,
    this.resetOTP,
  });

  @override
  _OTPResendState createState() => _OTPResendState();
}

class _OTPResendState extends State<OTPResend> {
  Timer _timer;
  int timerCount;
  int otpVerifyCount = 0;

  @override
  void initState() {
    super.initState();
    listenForOTP();
    startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();
    SmsAutoFill().unregisterListener();
    super.dispose();
  }

  void startTimer() {
    setState(() {
      timerCount = OTP_AWAIT_DURATION;
    });
    if (mounted) {
      setState(() {
        _timer = Timer.periodic(
          const Duration(seconds: 1),
          (Timer timer) => setState(
            () {
              timerCount < 1 ? timer.cancel() : timerCount -= 1;
            },
          ),
        );
      });
    }
  }

  void listenForOTP() async {
    await SmsAutoFill().listenForCode;
  }

  Future<void> onResendOTP() async {
    final AuthService authService = AuthService();
    widget.resetOTP();
    startTimer();
    listenForOTP();
    try {
      showLoading();
      await authService.resendConfirmationCode(
        mobile: widget.mobile,
      );
      hideLoading();
      getSnackbar(
        title: 'OTP resent Successfully',
      );
    } catch (_) {
      hideLoading();
      getSnackbar(
        title: 'Please try again later',
      );
    } finally {
      SmsAutoFill().unregisterListener();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 85.w(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Haven\'t received OTP?',
                style: failedOTP,
              ),
              TextButton(
                onPressed: timerCount == 0 ? onResendOTP : null,
                child: Text(
                  'Resend OTP',
                  style: timerCount == 0
                      ? TextStyle(
                          fontSize: resendTextFontSize,
                          color: accentColor,
                        )
                      : TextStyle(
                          fontSize: resendTextFontSize,
                          color: lightGreyColor,
                        ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

TextStyle timer = TextStyle(
  fontSize: 4.5.w(),
  color: primaryColor,
);

TextStyle failedOTP = TextStyle(
  fontSize: 3.5.w(),
);

TextStyle text = TextStyle(
  fontSize: 5.w(),
  color: primaryColor,
);

double resendTextFontSize = 3.5.w();
