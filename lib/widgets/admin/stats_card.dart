import 'package:flutter/material.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';

class StatsCard extends StatelessWidget {
  const StatsCard({
    Key key,
    @required this.title,
    @required this.data,
    @required this.onTap,
    @required this.path,
  }) : super(key: key);

  final String title;
  final int data;
  final Function onTap;
  final String path;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 14.h(),
        margin: EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 0,
        ),
        padding: EdgeInsets.fromLTRB(20, 5, 5, 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children: [
                SvgContainer(
                  height: 6,
                  width: 6,
                  color: Colors.white,
                  path: path,
                ),
                SizedBox(
                  width: 5.w(),
                ),
                AutoSizedText(
                  text: title,
                  height: 0.5.h(),
                  width: 8.w(),
                  textAlign: TextAlign.left,
                  align: Alignment.centerLeft,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                )
              ],
            ),
            AutoSizedText(
              text: data.toString(),
              height: 0.7.h(),
              width: 5.w(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xFF006BA6),
              Color(0xFF0496FF),
            ],
          ),
          borderRadius: BorderRadius.circular(
            12,
          ),
        ),
      ),
    );
  }
}
