import 'package:flutter/material.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:url_launcher/url_launcher.dart';

class OwnerCard extends StatelessWidget {
  const OwnerCard({
    Key key,
    @required this.name,
    @required this.count,
    @required this.mobile,
  }) : super(key: key);

  final String name;
  final String count;
  final String mobile;

  void callLead(String number) async {
    final url = 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      getSnackbar(title: 'Could not launch');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [
              Color(0xFF006BA6),
              Color(0xFF0496FF),
            ],
          ),
          borderRadius: BorderRadius.circular(
            12,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                AutoSizedText(
                  text: name,
                  align: Alignment.centerLeft,
                  width: 30,
                  height: 6,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                AutoSizedText(
                  align: Alignment.centerLeft,
                  text: '$count Stores',
                  width: 30,
                  height: 6,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                  ),
                ),
              ],
            ),
            SvgButton(
              color: Colors.white,
              height: 12,
              width: 10,
              path: 'assets/images/icons/call.svg',
              onTap: () {
                callLead(mobile);
              },
              edgeInsets: EdgeInsets.all(2),
            ),
          ],
        ),
      ),
    );
  }
}
