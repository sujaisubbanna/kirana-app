import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class CustomDropdown extends StatefulWidget {
  final String name;
  final String hint;
  final dynamic initialValue;
  final bool clear;
  final List<DropdownMenuItem<dynamic>> items;
  final String requiredErrorText;
  final Function onChanged;

  const CustomDropdown({
    Key key,
    this.name,
    this.hint,
    this.initialValue,
    this.clear,
    this.items,
    this.requiredErrorText,
    this.onChanged,
  }) : super(key: key);

  @override
  CustomDropdownState createState() => CustomDropdownState();
}

class CustomDropdownState extends State<CustomDropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80.w(),
      padding: EdgeInsets.all(
        3.w(),
      ),
      margin: EdgeInsets.all(
        3.w(),
      ),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
          4.w(),
        ),
        border: Border.all(
          color: Color(0xFF323F4B),
          width: 0.5,
        ),
        boxShadow: [
          BoxShadow(
            color: lightGreyColor.withOpacity(0.5),
            spreadRadius: 0.2,
            blurRadius: 1,
            offset: const Offset(1, 1),
          ),
        ],
      ),
      child: FormBuilderDropdown(
        name: widget.name,
        decoration: InputDecoration.collapsed(
          hintText: widget.hint,
          hintStyle: TextStyle(
            fontSize: 3.2.w(),
          ),
        ),
        onChanged: widget.onChanged,
        initialValue: widget.initialValue,
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(
            context,
            errorText: widget.requiredErrorText,
          ),
        ]),
        items: widget.items,
      ),
    );
  }
}
