import 'dart:convert';
import 'dart:io';

import 'package:image_cropper/image_cropper.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

class ImageCamera {
  final Function getImageBase64Format;

  ImageCamera({
    this.getImageBase64Format,
  });

  final ImagePicker picker = ImagePicker();

  Future getImage(String path) async {
    var croppedImage = await ImageCropper.cropImage(
      sourcePath: path,
      aspectRatio: CropAspectRatio(
        ratioX: 1,
        ratioY: 1,
      ),
      compressQuality: 100,
      maxHeight: 512,
      maxWidth: 512,
      compressFormat: ImageCompressFormat.jpg,
      androidUiSettings: AndroidUiSettings(
        toolbarColor: accentColor,
        toolbarTitle: 'Crop',
      ),
    );

    final File file = File(croppedImage.path);
    if (file.lengthSync() > MAX_IMAGE_FILE_SIZE) {
      getSnackbar(
        title: 'File too large, please select a smaller one',
      );
      return;
    }

    var base64ImageFormat = base64Encode(
      File(croppedImage.path).readAsBytesSync(),
    );

    String fileExtension = extension(croppedImage.path);
    fileExtension = fileExtension.replaceAll('.', '');
    base64ImageFormat = 'data:image/$fileExtension;base64, $base64ImageFormat';

    getImageBase64Format(
      base64ImageFormat,
    );
  }

  Future getImageFromCamera() async {
    try {
      final pickedFile = await picker.getImage(
        source: ImageSource.camera,
        preferredCameraDevice: CameraDevice.rear,
        imageQuality: 70,
      );
      if (pickedFile == null) return;
      return getImage(pickedFile.path);
    } on PlatformException catch (_) {
      getSnackbar(
        title: 'Please provide the permissions',
      );
    }
  }

  Future getImageFromGallery() async {
    try {
      final pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        imageQuality: 70,
      );

      if (pickedFile == null) return;
      return getImage(pickedFile.path);
    } on PlatformException catch (_) {
      getSnackbar(
        title: 'Please provide the permissions',
      );
    }
  }

  void showMyDialog() {
    final AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(
            5.w(),
          ),
        ),
      ),
      content: Container(
        alignment: Alignment.center,
        height: 10.h(),
        child: FittedBox(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 20.w(),
                ),
                child: Column(
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.perm_media,
                      ),
                      onPressed: () {
                        getImageFromGallery();
                        Get.back();
                      },
                      color: accentColor,
                      iconSize: 8.w(),
                    ),
                    Text(
                      'Gallery',
                      style: TextStyle(
                        fontSize: 3.w(),
                      ),
                    )
                  ],
                ),
              ),
              ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 20.w(),
                ),
                child: Column(
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.photo_camera,
                      ),
                      onPressed: () {
                        getImageFromCamera();
                        Get.back();
                      },
                      color: accentColor,
                      iconSize: 8.w(),
                    ),
                    Text(
                      'Camera',
                      style: TextStyle(
                        fontSize: 3.w(),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
    Get.dialog(alert);
  }
}
