import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class CustomInput extends StatefulWidget {
  final TextInputType keyboardType;
  final TextInputAction action;
  final String hint;
  final Function validator;
  final bool required;
  final String requiredErrorText;
  final int minLength;
  final int maxLength;
  final String name;
  final String initialValue;
  final String maxLengthErrorText;
  final String minLengthErrorText;
  final Function onTap;
  final int min;
  final int max;
  final String labelText;
  final bool email;
  final bool upi;
  final bool text;
  final bool ifsc;
  final double width;
  final bool lastField;

  const CustomInput({
    Key key,
    this.keyboardType = TextInputType.text,
    this.action = TextInputAction.done,
    this.hint = '',
    this.validator,
    this.required = true,
    this.requiredErrorText = '',
    this.minLength = -1,
    this.maxLength = -1,
    this.name,
    this.initialValue = '',
    this.onTap,
    this.min = 0,
    this.max,
    this.email = false,
    this.labelText,
    this.maxLengthErrorText = 'Please enter less characters',
    this.minLengthErrorText = 'Please enter more characters',
    this.upi = false,
    this.text = false,
    this.ifsc = false,
    this.width,
    this.lastField = false,
  }) : super(key: key);

  @override
  CustomInputState createState() => CustomInputState();
}

class CustomInputState extends State<CustomInput> {
  List<String Function(String)> validators = [];

  @override
  void initState() {
    super.initState();
    initValidators();
  }

  void initValidators() {
    final List<String Function(String)> temp = [];
    if (widget.required) {
      temp.add(
        FormBuilderValidators.required(
          context,
          errorText: widget.requiredErrorText,
        ),
      );
    }
    if (widget.upi) {
      temp.add(
        FormBuilderValidators.match(
          context,
          // ignore: unnecessary_string_escapes
          '[\w.-]*[@][\w]*',
          errorText: 'Please enter valid UPI code',
        ),
      );
    }
    if (widget.ifsc) {
      temp.add(
        FormBuilderValidators.match(
          context,
          // ignore: unnecessary_string_escapes
          '^[A-Z]{4}[0][A-Z0-9]{6}',
          errorText: 'Please enter valid IFSC code',
        ),
      );
    }

    if (widget.keyboardType == TextInputType.number) {
      temp.add(
        FormBuilderValidators.numeric(
          context,
          errorText: 'Please enter a valid number',
        ),
      );
      temp.add(
        FormBuilderValidators.min(
          context,
          widget.min,
          errorText: 'Please enter number more than ${widget.min}',
        ),
      );
      if (widget.max != null) {
        temp.add(
          FormBuilderValidators.max(
            context,
            widget.max,
            errorText: 'Please enter number less than ${widget.max}',
          ),
        );
      }
    }
    temp.add(
      FormBuilderValidators.match(
        context,
        r'^[^!#$%^&*()?":{}|<>;]*$',
        errorText: 'No special characters allowed',
      ),
    );
    if (widget.email) {
      temp.add(
        FormBuilderValidators.email(
          context,
          errorText: 'Please enter valid email ID',
        ),
      );
    }
    if (widget.minLength > 0) {
      temp.add(
        FormBuilderValidators.minLength(
          context,
          widget.minLength,
          errorText: widget.minLengthErrorText,
        ),
      );
    }
    if (widget.maxLength > 0) {
      temp.add(
        FormBuilderValidators.maxLength(
          context,
          widget.maxLength,
          errorText: widget.maxLengthErrorText,
        ),
      );
    }
    setState(() {
      validators = temp;
    });
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Container(
      width: widget.width != null ? widget.width : 80.w(),
      padding: EdgeInsets.symmetric(
        horizontal: 3.5.w(),
      ),
      margin: EdgeInsets.all(
        2.w(),
      ),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
          4.w(),
        ),
        border: Border.all(
          color: Color(0xFF323F4B),
          width: 0.5,
        ),
        boxShadow: [
          BoxShadow(
            color: lightGreyColor.withOpacity(0.5),
            spreadRadius: 0.2,
            blurRadius: 1,
            offset: const Offset(1, 1),
          ),
        ],
      ),
      child: FormBuilderTextField(
        inputFormatters: widget.text
            ? [
                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z ]')),
              ]
            : [],
        textCapitalization: widget.ifsc
            ? TextCapitalization.characters
            : TextCapitalization.none,
        name: widget.name,
        keyboardType: widget.keyboardType,
        initialValue: widget.initialValue,
        textInputAction: widget.action,
        onTap: widget.onTap != null ? () => widget.onTap() : () {},
        style: TextStyle(
          fontSize: 4.w(),
        ),
        onEditingComplete: widget.lastField
            ? () {
                FocusScope.of(context).unfocus();
              }
            : () => node.nextFocus(),
        decoration: InputDecoration(
          labelText: widget.labelText,
          labelStyle: TextStyle(
            fontSize: 4.w(),
          ),
          border: InputBorder.none,
          counterText: '',
          hintText: widget.hint,
          hintStyle: TextStyle(
            fontSize: 3.2.w(),
          ),
        ),
        validator: FormBuilderValidators.compose(validators),
      ),
    );
  }
}
