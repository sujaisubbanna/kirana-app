import 'package:skipyq_app/config/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SvgContainer extends StatelessWidget {
  final double width;
  final double height;
  final String path;
  final Color color;

  const SvgContainer({
    @required this.width,
    @required this.height,
    @required this.path,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width * SizeConfig.safeBlockHorizontal,
      height: height * SizeConfig.safeBlockVertical,
      child: FittedBox(
        child: SvgPicture.asset(
          path,
          color: color,
        ),
      ),
    );
  }
}
