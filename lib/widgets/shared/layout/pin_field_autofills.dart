import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sms_autofill/sms_autofill.dart';

class PinFieldAutoFills extends StatefulWidget {
  final int codeLength;
  final bool autofocus;
  final TextEditingController controller;
  final String currentCode;
  final Function(String) onCodeSubmitted;
  final Function(String) onCodeChanged;
  final PinDecoration decoration;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;

  const PinFieldAutoFills({
    Key key,
    this.keyboardType = TextInputType.number,
    this.textInputAction = TextInputAction.done,
    this.focusNode,
    this.controller,
    this.decoration,
    this.onCodeSubmitted,
    this.onCodeChanged,
    this.currentCode,
    this.autofocus = false,
    this.codeLength = 6,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PinFieldAutoFillState();
  }
}

class _PinFieldAutoFillState extends State<PinFieldAutoFills>
    with CodeAutoFill {
  TextEditingController controller;
  bool _shouldDisposeController;

  @override
  Widget build(BuildContext context) {
    return PinInputTextField(
      pinLength: widget.codeLength,
      decoration: widget.decoration,
      focusNode: widget.focusNode,
      enableInteractiveSelection: true,
      textCapitalization: TextCapitalization.characters,
      toolbarOptions: const ToolbarOptions(paste: true),
      keyboardType: widget.keyboardType,
      autoFocus: widget.autofocus,
      controller: controller,
      textInputAction: widget.textInputAction,
      onSubmit: widget.onCodeSubmitted,
      inputFormatters: [
        UpperCaseTextFormatter(),
      ],
    );
  }

  @override
  void initState() {
    _shouldDisposeController = widget.controller == null;
    controller = widget.controller ?? TextEditingController(text: '');
    code = widget.currentCode;
    codeUpdated();
    controller.addListener(() {
      if (controller.text != code) {
        code = controller.text;
        if (widget.onCodeChanged != null) {
          widget.onCodeChanged(code);
        }
      }
    });
    listenForCode();
    super.initState();
  }

  @override
  void codeUpdated() {
    if (controller.text != code) {
      controller.value = TextEditingValue(text: code ?? '');
      if (widget.onCodeChanged != null) {
        widget.onCodeChanged(code ?? '');
      }
    }
  }

  @override
  void dispose() {
    cancel();
    if (_shouldDisposeController) {
      controller.dispose();
    }
    unregisterListener();
    super.dispose();
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text?.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
