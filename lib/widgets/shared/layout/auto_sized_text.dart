import 'package:auto_size_text/auto_size_text.dart';
import 'package:skipyq_app/config/size_config.dart';
import 'package:flutter/material.dart';

class AutoSizedText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final bool multilineEnable;
  final double width;
  final double height;
  final Alignment align;
  final int maxlines;
  final TextAlign textAlign;
  final double minFontSize;
  final AutoSizeGroup group;
  final double maxFontSize;

  const AutoSizedText({
    @required this.text,
    this.multilineEnable = false,
    this.style,
    this.width,
    this.height,
    this.align = Alignment.center,
    this.maxlines = 4,
    this.textAlign = TextAlign.center,
    this.minFontSize = 10,
    this.maxFontSize = 24,
    this.group,
  });

  @override
  Widget build(BuildContext context) {
    return multilineEnable
        ? Container(
            width: width * SizeConfig.safeBlockHorizontal,
            height: height * SizeConfig.safeBlockVertical,
            alignment: align,
            child: AutoSizeText(
              text,
              style: style,
              textAlign: textAlign,
              minFontSize: minFontSize,
              maxFontSize: maxFontSize,
              maxLines: maxlines,
              overflow: TextOverflow.fade,
            ),
          )
        : text != null || text != ''
            ? Container(
                width: width * SizeConfig.safeBlockHorizontal,
                height: height * SizeConfig.safeBlockVertical,
                alignment: align,
                child: FittedBox(
                  child: AutoSizeText(
                    text,
                    minFontSize: minFontSize,
                    maxFontSize: maxFontSize,
                    group: group,
                    style: style,
                  ),
                ),
              )
            : Container(
                width: width * SizeConfig.safeBlockHorizontal,
                height: height * SizeConfig.safeBlockVertical,
              );
  }
}
