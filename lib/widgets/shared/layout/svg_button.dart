import 'package:flutter/material.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';

class SvgButton extends StatelessWidget {
  final Function onTap;
  final Color color;
  final String path;
  final double height;
  final double width;
  final EdgeInsets edgeInsets;

  SvgButton({
    this.onTap,
    this.color,
    this.path,
    this.height,
    this.width,
    this.edgeInsets,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: edgeInsets,
        child: SvgContainer(
          height: height,
          width: width,
          path: path,
          color: color,
        ),
      ),
    );
  }
}
