import 'package:flutter/material.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'auto_sized_text.dart';

class CustomButton extends StatelessWidget {
  final bool solid;
  final String text;
  final Widget icon;
  final Function onTap;
  final double width;
  final double height;
  final Color color;

  CustomButton({
    @required this.solid,
    @required this.text,
    this.icon,
    @required this.onTap,
    @required this.height,
    @required this.width,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return solid
        ? GestureDetector(
            onTap: onTap,
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                color: color != null ? color : accentColor,
                borderRadius: BorderRadius.circular(
                  50,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.25),
                    spreadRadius: 0,
                    blurRadius: 4,
                    offset: const Offset(0, 4),
                  ),
                ],
              ),
              padding: EdgeInsets.symmetric(
                horizontal: 4.w(),
              ),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  icon != null
                      ? icon
                      : Container(
                          child: null,
                        ),
                  icon != null
                      ? SizedBox(
                          width: 3.w(),
                        )
                      : Container(
                          child: null,
                        ),
                  AutoSizedText(
                    text: text,
                    style: buttonText,
                    height: height * 0.7,
                    width: width * 0.17,
                  ),
                ],
              ),
            ),
          )
        : GestureDetector(
            onTap: onTap,
            child: Container(
              width: width,
              height: height,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  50,
                ),
                border: Border.all(
                  color: color != null ? color : Color(0xFF353535),
                  width: 0.5,
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  icon != null
                      ? icon
                      : Container(
                          child: null,
                        ),
                  icon != null
                      ? SizedBox(
                          width: 3.w(),
                        )
                      : Container(
                          child: null,
                        ),
                  AutoSizedText(
                    text: text,
                    height: height * 0.7,
                    width: width * 0.17,
                    style: TextStyle(
                      color: color != null ? color : Color(0xFF353535),
                      fontWeight: FontWeight.w700,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}

TextStyle buttonText = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w600,
  color: Colors.white,
);
