import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';

DateTime currentBackPressTime;

Future<bool> onCloseApp() {
  final DateTime now = DateTime.now();
  if (currentBackPressTime == null ||
      now.difference(currentBackPressTime) > const Duration(seconds: 2)) {
    currentBackPressTime = now;
    Get.dialog(
      CustomAlertDialog(
        title: 'Do you want to close the app ?',
        confirmTitle: 'Exit',
        onConfirm: () =>
            SystemChannels.platform.invokeMethod('SystemNavigator.pop'),
        returnTitle: 'Back',
        onReturn: onReturn,
      ),
    );
    return Future.value(false);
  }
  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  return Future.value(true);
}

void onReturn() {
  if (Get.isDialogOpen) Get.close(1);
}
