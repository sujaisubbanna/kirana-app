import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter/material.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';

class CustomAlertDialog extends StatelessWidget {
  final String title;
  final String confirmTitle;
  final String returnTitle;
  final Function onConfirm;
  final Function onReturn;
  final bool dismissable;

  const CustomAlertDialog({
    Key key,
    this.title,
    this.confirmTitle,
    this.returnTitle,
    this.onConfirm,
    this.onReturn,
    this.dismissable = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(dismissable);
      },
      child: AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(
              5.w(),
            ),
          ),
        ),
        contentPadding: EdgeInsets.all(
          4.w(),
        ),
        content: Container(
          height: 23.h(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    Icons.info_outline,
                    size: 8.w(),
                  ),
                  SizedBox(
                    width: 1.w(),
                  ),
                  Expanded(
                    child: AutoSizedText(
                      text: title,
                      multilineEnable: true,
                      height: 2.h(),
                      width: 30.w(),
                      style: titleStyle,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  onReturn != null
                      ? TextButton(
                          onPressed: () => onReturn(),
                          child: Text(
                            returnTitle,
                            style: returnStyle,
                          ),
                        )
                      : Container(),
                  onConfirm != null
                      ? MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                5.w(),
                              ),
                            ),
                          ),
                          color: accentColor,
                          onPressed: () => onConfirm(),
                          child: Text(
                            confirmTitle,
                            style: confirmStyle,
                          ),
                        )
                      : Container(),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

TextStyle titleStyle = TextStyle(
  fontSize: 4.w(),
);

TextStyle returnStyle = TextStyle(
  color: accentColor,
  fontSize: 3.5.w(),
  fontWeight: FontWeight.w800,
);

TextStyle confirmStyle = TextStyle(
  color: lightColor,
  fontSize: 3.5.w(),
);
