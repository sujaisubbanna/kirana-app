import 'package:skipyq_app/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void getSnackbar({
  String title,
  Color color = Colors.white,
  ToastGravity gravity = ToastGravity.TOP,
}) {
  Fluttertoast.showToast(
    msg: title,
    toastLength: Toast.LENGTH_SHORT,
    gravity: gravity,
    backgroundColor: color,
    textColor: accentColor,
  );
}
