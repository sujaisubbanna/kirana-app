import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:skipyq_app/models/shared/order_status_enum.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:recase/recase.dart';

class OrderStatusChip extends StatelessWidget {
  final UtilsService utilsService = UtilsService();
  final OrderStatus status;
  final double height;
  final double weight;

  OrderStatusChip({
    @required this.status,
    this.height,
    this.weight,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 1.w(),
      ),
      decoration: BoxDecoration(
        color: utilsService.getOrderStatusColor(status),
        borderRadius: BorderRadius.circular(
          20,
        ),
      ),
      child: AutoSizedText(
        text: EnumToString.convertToString(status).titleCase,
        width: height != null ? height : 8.w(),
        height: weight != null ? weight : 0.6.h(),
        align: Alignment.center,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
