import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/models/admin/stats_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/models/shared/time_period_enum.dart';
import 'package:skipyq_app/services/admin/admin_service.dart';
import 'package:skipyq_app/widgets/admin/stats_card.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:toggle_bar/toggle_bar.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with AfterLayoutMixin<DashboardScreen> {
  TimePeriod timePeriod = TimePeriod.LAST_24_HRS;
  Stats stats;
  AdminService adminService = new AdminService();

  @override
  void afterFirstLayout(BuildContext context) {
    getStats(timePeriod);
  }

  void getStats(TimePeriod timePeriod) async {
    showLoading();
    try {
      Stats temp = await adminService.getStats(timePeriod);
      setState(() {
        stats = temp;
      });
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void addOwner() {
    Get.toNamed(routes.ADD_OWNER);
  }

  void setFilter(int index) {
    TimePeriod temp;
    switch (index) {
      case 0:
        temp = TimePeriod.LAST_24_HRS;
        break;
      case 1:
        temp = TimePeriod.LAST_WEEK;
        break;
      case 2:
        temp = TimePeriod.LAST_MONTH;
        break;
      case 3:
        temp = TimePeriod.ALL_TIME;
        break;
      default:
        temp = TimePeriod.LAST_24_HRS;
    }
    setState(() {
      timePeriod = temp;
    });
    getStats(temp);
  }

  void logout() {
    Get.find<AppController>().logout();
    Get.offAllNamed(routes.ROLE_SELECT);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          title: Text(
            'Dashboard',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
          actions: [
            SvgButton(
              color: Color(
                0xFF365E7D,
              ),
              path: 'assets/images/icons/logout.svg',
              height: 10,
              width: 7,
              onTap: logout,
              edgeInsets: EdgeInsets.all(8.0),
            ),
            SvgButton(
              color: Color(
                0xFF365E7D,
              ),
              path: 'assets/images/icons/refresh.svg',
              height: 10,
              width: 7,
              onTap: () {
                getStats(timePeriod);
              },
              edgeInsets: EdgeInsets.all(8.0),
            ),
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ToggleBar(
              backgroundColor: accentColor,
              textColor: Colors.white,
              selectedTabColor: Colors.white,
              selectedTextColor: accentColor,
              labels: [
                '24 Hrs',
                'Week',
                'Month',
                'All Time',
              ],
              onSelectionUpdated: setFilter,
            ),
            stats != null
                ? Container(
                    height: 60.h(),
                    width: 90.w(),
                    child: ListView(
                      children: [
                        StatsCard(
                          title: 'Customers',
                          data: stats.customers,
                          onTap: null,
                          path: 'assets/images/icons/customers.svg',
                        ),
                        StatsCard(
                          title: 'Stores',
                          data: stats.stores,
                          onTap: null,
                          path: 'assets/images/icons/stores.svg',
                        ),
                        StatsCard(
                          title: 'Orders',
                          data: stats.orders,
                          onTap: null,
                          path: 'assets/images/icons/orders.svg',
                        ),
                        StatsCard(
                          title: 'Owners',
                          data: stats.owners,
                          onTap: () {
                            Get.toNamed(routes.OWNERS);
                          },
                          path: 'assets/images/icons/owners.svg',
                        ),
                        SizedBox(
                          height: 1.h(),
                        ),
                      ],
                    ),
                  )
                : Container(
                    height: 60.h(),
                    width: 90.w(),
                  ),
            CustomButton(
              icon: SvgContainer(
                path: 'assets/images/icons/add_owner.svg',
                height: 8,
                width: 5,
                color: Color(0xFF353535),
              ),
              text: 'ADD OWNER',
              solid: false,
              onTap: addOwner,
              width: 85.w(),
              height: 7.h(),
            )
          ],
        ),
      ),
    );
  }
}
