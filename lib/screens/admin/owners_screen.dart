import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/admin_controller.dart';
import 'package:skipyq_app/models/shared/identity_model.dart';
import 'package:skipyq_app/services/admin/admin_service.dart';
import 'package:skipyq_app/widgets/admin/owner_card.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class OwnersScreen extends StatefulWidget {
  @override
  _OwnersScreenState createState() => _OwnersScreenState();
}

class _OwnersScreenState extends State<OwnersScreen>
    with AfterLayoutMixin<OwnersScreen> {
  AdminService adminService = new AdminService();

  @override
  void afterFirstLayout(BuildContext context) {
    if (Get.find<AdminController>().owners.length == 0) {
      getOwners();
    }
  }

  void getOwners() async {
    try {
      showLoading();
      await this.adminService.getOwners();
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: accentColor,
        title: Text(
          'Owners',
        ),
        actions: [
          SvgButton(
            color: Colors.white,
            height: 8,
            width: 8,
            edgeInsets: EdgeInsets.all(8),
            onTap: getOwners,
            path: 'assets/images/icons/refresh.svg',
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GetBuilder<AdminController>(
          builder: (value) {
            return value.owners.length > 0
                ? ListView(
                    children: value.owners
                        .map(
                          (e) => OwnerCard(
                            count: e.stores.length.toString(),
                            name: e.name,
                            mobile: (e.identity as Identity).mobile,
                          ),
                        )
                        .toList(),
                  )
                : Center(
                    child: Text('No Owners found'),
                  );
          },
        ),
      ),
    );
  }
}
