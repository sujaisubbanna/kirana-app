import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:skipyq_app/services/admin/admin_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class AddOwnerScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  final AdminService adminService = new AdminService();

  void addOwner() async {
    if (_formKey.currentState.saveAndValidate()) {
      showLoading();
      final Map<String, dynamic> value = _formKey.currentState.value;
      await adminService.createOwner(
        name: value['name'] as String,
        mobile: value['mobile'] as String,
      );
      await adminService.getOwners();
      hideLoading();
      Get.back();
    } else {
      hideLoading();
      getSnackbar(title: 'Check Owner Details');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: accentColor,
        title: Text(
          'Add Owner',
        ),
      ),
      body: Column(
        children: [
          FormBuilder(
            key: _formKey,
            initialValue: {
              'mobile': '',
              'name': '',
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                5.w(),
                10.w(),
                5.w(),
                0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const CustomInput(
                    minLength: 3,
                    labelText: 'Name',
                    maxLength: 100,
                    name: 'name',
                    requiredErrorText: 'Please enter name',
                    text: true,
                  ),
                  const CustomInput(
                    maxLength: 10,
                    labelText: 'Phone Number',
                    minLength: 10,
                    minLengthErrorText: 'Please enter valid phone number',
                    requiredErrorText: 'Please enter phone number',
                    name: 'mobile',
                    maxLengthErrorText: 'Please enter valid phone number',
                    keyboardType: TextInputType.number,
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          CustomButton(
            onTap: addOwner,
            text: 'SUBMIT',
            solid: true,
            icon: null,
            height: 8.h(),
            width: 75.w(),
          ),
          SizedBox(
            height: 2.h(),
          ),
        ],
      ),
    );
  }
}
