import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class FeedbackScreen extends StatefulWidget {
  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  final _feedbackFormKey = GlobalKey<FormBuilderState>();
  final CustomerService customerService = CustomerService();
  Roles role;

  @override
  void initState() {
    role = Get.arguments as Roles;
    super.initState();
  }

  void submitFeedback() async {
    if (_feedbackFormKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final Map<String, dynamic> values = _feedbackFormKey.currentState.value;
        await this.customerService.feedback(values['feedback'], role);
        hideLoading();
        Get.back();
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Please try again later.');
      }
    } else {
      getSnackbar(title: 'Check feedback field');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Feedback',
        ),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 30.h(),
              width: 90.w(),
              child: FormBuilder(
                key: _feedbackFormKey,
                child: ListView(
                  children: [
                    CustomInput(
                      minLength: 3,
                      labelText: 'Feedback',
                      name: 'feedback',
                      lastField: true,
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
            CustomButton(
              solid: true,
              text: 'SUBMIT',
              onTap: submitFeedback,
              height: 7.h(),
              width: 80.w(),
              color: accentColor,
            ),
            SizedBox(
              height: 2.h(),
            ),
          ],
        ),
      ),
    );
  }
}
