import 'package:after_layout/after_layout.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config.dart';
import 'package:skipyq_app/controllers/shared/customer_controller.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/customer/current_order_card.dart';
import 'package:skipyq_app/widgets/customer/customer_drawer.dart';
import 'package:skipyq_app/widgets/customer/customer_store_card.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:chips_choice/chips_choice.dart';

class CustomerHomeScreen extends StatefulWidget {
  @override
  _CustomerHomeScreenState createState() => _CustomerHomeScreenState();
}

class _CustomerHomeScreenState extends State<CustomerHomeScreen>
    with AfterLayoutMixin<CustomerHomeScreen> {
  var storeCardTitleGroup = AutoSizeGroup();
  final CustomerService customerService = CustomerService();
  final UtilsService utilsService = UtilsService();
  TextEditingController searchTextController = TextEditingController();
  String searchTerm = '';
  List<String> tags = [];
  List<Store> filteredStores = [];

  @override
  void afterFirstLayout(BuildContext context) {
    if (Get.find<CustomerController>().stores.length == 0) {
      getStores();
    }
    getFilteredStores();
  }

  void onSearchTextChanged(String text) async {
    if (text.isEmpty) {
      setState(() {
        searchTerm = '';
      });
      return;
    } else {
      setState(() {
        searchTerm = text.toLowerCase();
      });
    }
    getFilteredStores();
  }

  Widget getSearchBox() {
    return Container(
      color: accentColor,
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 2,
        child: ListTile(
          visualDensity: const VisualDensity(
            horizontal: -4,
            vertical: -4,
          ),
          leading: const Icon(Icons.search),
          title: TextField(
            controller: searchTextController,
            decoration: InputDecoration(
              hintText: 'Store name',
              hintStyle: TextStyle(
                fontSize: 3.5 * SizeConfig.safeBlockHorizontal,
              ),
              border: InputBorder.none,
            ),
            onChanged: onSearchTextChanged,
          ),
          trailing: IconButton(
            icon: const Icon(Icons.cancel),
            onPressed: () {
              searchTextController.clear();
              onSearchTextChanged('');
            },
          ),
        ),
      ),
    );
  }

  void getFilteredStores() {
    List<Store> temp = Get.find<CustomerController>().stores;
    if (tags.isNotEmpty) {
      temp = temp
          .where(
            (x) => tags.contains(
              EnumToString.convertToString(x.type),
            ),
          )
          .toList();
    }
    if (searchTerm.isNotEmpty) {
      temp =
          temp.where((x) => x.name.toLowerCase().contains(searchTerm)).toList();
    }
    setState(() {
      filteredStores = temp;
    });
  }

  void getStores() async {
    try {
      showLoading();
      await customerService.getStores();
      getFilteredStores();
      hideLoading();
    } catch (e) {
      print(e);
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(
        width: 50.w(),
        child: CustomerDrawerWidget(),
      ),
      appBar: AppBar(
        title: Text('Home'),
        shadowColor: Colors.transparent,
        actions: [
          SvgButton(
            color: Colors.white,
            path: 'assets/images/icons/refresh.svg',
            height: 10,
            width: 7,
            onTap: getStores,
            edgeInsets: EdgeInsets.all(8.0),
          ),
        ],
      ),
      body: Stack(
        children: [
          Column(
            children: [
              getSearchBox(),
              Container(
                width: 100.w(),
                color: accentColor,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ChipsChoice<String>.multiple(
                          choiceStyle: C2ChoiceStyle(
                            borderColor: primaryColor,
                            borderWidth: 0.5,
                            elevation: 2,
                            labelStyle: TextStyle(
                              color: primaryColor,
                            ),
                          ),
                          choiceActiveStyle: C2ChoiceStyle(
                            borderColor: accentColor,
                            borderWidth: 0.5,
                            elevation: 2,
                            labelStyle: TextStyle(
                              color: accentColor,
                            ),
                          ),
                          value: tags,
                          onChanged: (val) => setState(() {
                            tags = val;
                            getFilteredStores();
                          }),
                          choiceItems: C2Choice.listFrom<String, String>(
                            source: StoreType.values
                                .map(
                                  (x) => EnumToString.convertToString(x),
                                )
                                .toList(),
                            value: (i, v) => v,
                            label: (i, v) => v,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      child: AutoSizedText(
                        text: 'Stores near you',
                        height: 0.6.h(),
                        width: 10.w(),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              filteredStores.length > 0
                  ? Expanded(
                      child: ListView(
                        children: filteredStores.map(
                          (e) {
                            return CustomerStoreCard(
                              titleGroup: storeCardTitleGroup,
                              store: e,
                            );
                          },
                        ).toList(),
                      ),
                    )
                  : Center(
                      child: Container(
                        child: Text('No stores found'),
                      ),
                    ),
            ],
          ),
          CurrentOrderCard(),
        ],
      ),
    );
  }
}
