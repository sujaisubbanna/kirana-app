import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/customer/order_item_field.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class CustomerCreateOrderScreen extends StatefulWidget {
  @override
  _CustomerCreateOrderScreenState createState() =>
      _CustomerCreateOrderScreenState();
}

class _CustomerCreateOrderScreenState extends State<CustomerCreateOrderScreen>
    with AfterLayoutMixin<CustomerCreateOrderScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final UtilsService utilsService = UtilsService();
  final CustomerService customerService = CustomerService();
  ScrollController itemController = ScrollController();

  List<OrderItemField> fields = [];
  Store store;

  @override
  void initState() {
    store = Get.arguments as Store;
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    List<OrderItemField> temp = [
      OrderItemField(
        idx: 0,
        formKey: _formKey,
        storeType: store.type,
      ),
    ];
    _formKey.currentState.setInternalFieldValue('qty_0', '1');
    setState(() {
      fields = temp;
    });
  }

  List<Map<String, dynamic>> getOrderDetails(Map<String, dynamic> values) {
    List<Map<String, dynamic>> temp = [];
    final length = (values.length - 1) / 2;
    for (var i = 0; i < length; i++) {
      if ((values['name_$i'] as String).isNotEmpty) {
        temp.add({
          'name': values['name_$i'],
          'qty': values['qty_$i'],
          'price': 0.0,
        });
      }
    }
    return temp;
  }

  void createOrder() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final List<Map<String, dynamic>> data =
            getOrderDetails(_formKey.currentState.value);
        final notes = _formKey.currentState.value['notes'];
        Order order = await customerService.createOrder(store.id, data, notes);
        hideLoading();
        Get.offAndToNamed(
          routes.CUSTOMER_ORDER,
          arguments: {'order': order, 'orderType': OrderType.OPEN},
        );
      } catch (e) {
        print(e);
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    } else {
      getSnackbar(title: 'Check order details');
    }
  }

  void addItem() {
    List<OrderItemField> temp = fields;
    _formKey.currentState.setInternalFieldValue('qty_${temp.length}', '1');
    temp.add(OrderItemField(
      idx: temp.length,
      formKey: _formKey,
      storeType: store.type,
    ));
    setState(() {
      fields = temp;
    });
    SchedulerBinding.instance.addPostFrameCallback((_) {
      itemController.animateTo(
        itemController.position.maxScrollExtent,
        duration: Duration(milliseconds: 200),
        curve: Curves.easeInOut,
      );
    });
  }

  void removeItem() {
    if (fields.length > 1) {
      List<OrderItemField> temp = fields;
      temp.removeLast();
      setState(() {
        fields = temp;
      });
    } else {
      getSnackbar(title: 'Need at least 1 item in order.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'Create Order',
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.remove_circle,
            ),
            onPressed: removeItem,
          ),
          IconButton(
            icon: Icon(
              Icons.add_circle_rounded,
            ),
            onPressed: addItem,
          ),
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            AutoSizedText(
              text: store.name,
              height: 0.7.h(),
              width: 20.w(),
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 20,
              ),
            ),
            AutoSizedText(
              text: store.location.address,
              height: 0.8.h(),
              width: 20.w(),
              multilineEnable: true,
            ),
            FormBuilder(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  0.5.w(),
                  0.w(),
                  0.5.w(),
                  0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 55.h(),
                      child: SingleChildScrollView(
                        controller: itemController,
                        child: Column(
                          children: fields,
                        ),
                      ),
                    ),
                    CustomInput(
                      labelText: 'Notes (Optional)',
                      maxLength: 100,
                      name: 'notes',
                      hint: utilsService.getStoreNotes(store.type),
                      width: 80.w(),
                      required: false,
                      lastField: true,
                    ),
                    AutoSizedText(
                      text: 'Orders cannot be canceled once placed',
                      height: 0.5.h(),
                      width: 20.w(),
                      multilineEnable: true,
                      style: TextStyle(
                        color: accentColor,
                      ),
                    ),
                    SizedBox(
                      height: 1.h(),
                    ),
                    CustomButton(
                      text: 'CREATE ORDER',
                      height: 7.h(),
                      width: 80.w(),
                      solid: true,
                      onTap: () {
                        if (_formKey.currentState.saveAndValidate()) {
                          Get.dialog(
                            CustomAlertDialog(
                              title: 'Do you want to place the order?',
                              onReturn: () => Get.back(),
                              onConfirm: () {
                                Get.back();
                                createOrder();
                              },
                              returnTitle: 'Cancel',
                              confirmTitle: 'Create',
                            ),
                          );
                        } else {
                          getSnackbar(title: 'Check order details');
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
