import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_status_enum.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/order/order_status.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomerOrderScreen extends StatefulWidget {
  @override
  _CustomerOrderScreenState createState() => _CustomerOrderScreenState();
}

class _CustomerOrderScreenState extends State<CustomerOrderScreen> {
  final CustomerService customerService = CustomerService();
  Order order;
  OrderType orderType;

  @override
  void initState() {
    Map<String, dynamic> arguments = Get.arguments;
    order = arguments['order'] as Order;
    orderType = arguments['orderType'] as OrderType;
    super.initState();
  }

  void launchMapsUrl(
    List<double> source,
    List<double> destination,
  ) async {
    String mapOptions = [
      'saddr=${source[1]},${source[0]}',
      'daddr=${destination[1]},${destination[0]}',
      'dir_action=navigate'
    ].join('&');

    final url = 'https://www.google.com/maps?$mapOptions';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      getSnackbar(title: 'Could not launch maps');
    }
  }

  void getOrder() async {
    try {
      showLoading();
      Order temp = await customerService.getOrder(order.id, orderType);
      setState(() {
        order = temp;
      });
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void callOwner(String number) async {
    final url = 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      getSnackbar(title: 'Could not launch');
    }
  }

  void viewPaymentDetails() {
    if ((order.store as Store).paymentDetails != null) {
      Get.toNamed(routes.CUSTOMER_PAYMENT_DETAILS,
          arguments: (order.store as Store).paymentDetails);
    } else {
      getSnackbar(title: 'Payment details not found.');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Details'),
        actions: [
          SvgButton(
            color: Colors.white,
            path: 'assets/images/icons/refresh.svg',
            height: 10,
            width: 7,
            onTap: getOrder,
            edgeInsets: EdgeInsets.all(8.0),
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 2.h(),
            ),
            AutoSizedText(
              text: '#${order.orderNo.toString()}',
              height: 0.6.h(),
              width: 12.w(),
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 2.h(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomButton(
                  height: 6.h(),
                  width: 45.w(),
                  icon: Icon(
                    Icons.location_on,
                    color: accentColor,
                  ),
                  text: 'GET DIRECTIONS',
                  solid: false,
                  onTap: () {
                    List<double> source = Get.find<UserController>()
                        .customer
                        .location
                        .geoLocation
                        .coordinates;
                    List<double> destination =
                        (order.store as Store).location.geoLocation.coordinates;
                    launchMapsUrl(source, destination);
                  },
                  color: accentColor,
                ),
                if (order.status == OrderStatus.AWAITING_PAYMENT)
                  CustomButton(
                    height: 6.h(),
                    width: 35.w(),
                    icon: Icon(
                      Icons.phone,
                      color: accentColor,
                    ),
                    text: 'CALL STORE',
                    solid: false,
                    onTap: () {
                      callOwner((order.store as Store).contactNo);
                    },
                    color: accentColor,
                  ),
              ],
            ),
            SizedBox(
              height: 1.h(),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 3.w(),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  AutoSizedText(
                    text: (order.store as Store).name,
                    height: 1.h(),
                    width: 12.w(),
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                    multilineEnable: true,
                  ),
                  OrderStatusChip(
                    status: order.status,
                  ),
                ],
              ),
            ),
            AutoSizedText(
              text: 'Items',
              width: 5.w(),
              height: 1.h(),
              align: Alignment.center,
              style: TextStyle(
                fontSize: 20,
                color: accentColor,
              ),
            ),
            Container(
              height: 36.h(),
              color: Color(0xFF),
              child: ListView(
                children: order.items
                    .map(
                      (e) => Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizedText(
                            text: e.name,
                            width: 10.w(),
                            height: 1.h(),
                            align: Alignment.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                            multilineEnable: true,
                          ),
                          AutoSizedText(
                            text: 'Qty. ${e.qty.toString()}',
                            width: 5.w(),
                            height: 1.h(),
                            align: Alignment.centerLeft,
                          ),
                          if ((order.status == OrderStatus.PACKED ||
                                  order.status == OrderStatus.COMPLETED) &&
                              e.price > 0)
                            AutoSizedText(
                              text: '₹ ${e.price}',
                              width: 8.w(),
                              height: 0.6.h(),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          if ((order.status == OrderStatus.PACKED ||
                                  order.status == OrderStatus.COMPLETED) &&
                              e.price == 0)
                            AutoSizedText(
                              text: 'Not available',
                              width: 8.w(),
                              height: 0.6.h(),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                        ],
                      ),
                    )
                    .toList(),
              ),
            ),
            if (order.notes != null && order.notes.isNotEmpty)
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: AutoSizedText(
                  text: 'NOTES: ${order.notes}',
                  height: 0.5.h(),
                  width: 80.w(),
                  multilineEnable: true,
                  align: Alignment.centerLeft,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                ),
              ),
            Spacer(),
            if (order.status == OrderStatus.PACKED ||
                order.status == OrderStatus.COMPLETED ||
                order.status == OrderStatus.AWAITING_PAYMENT)
              Container(
                color: accentColor,
                padding: EdgeInsets.symmetric(
                  vertical: 4,
                  horizontal: 5.w(),
                ),
                child: Column(
                  children: [
                    if (order.discount > 0)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizedText(
                            text: 'Gross Amount',
                            width: 8.w(),
                            height: 0.5.h(),
                            align: Alignment.centerLeft,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          AutoSizedText(
                            text: '₹ ${order.grossAmount.toString()}',
                            width: 8.w(),
                            height: 0.5.h(),
                            align: Alignment.centerRight,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    if (order.discount > 0)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizedText(
                            text: 'Discount',
                            width: 8.w(),
                            height: 0.5.h(),
                            align: Alignment.centerLeft,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          AutoSizedText(
                            text: '₹ ${order.discount.toString()}',
                            width: 8.w(),
                            height: 0.5.h(),
                            align: Alignment.centerRight,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizedText(
                          text: 'Net Amount',
                          width: 8.w(),
                          height: 0.5.h(),
                          align: Alignment.centerLeft,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        AutoSizedText(
                          text: '₹ ${order.netAmount.toString()}',
                          width: 8.w(),
                          height: 0.5.h(),
                          align: Alignment.centerRight,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            Spacer(),
            if ((order.status == OrderStatus.PACKED ||
                    order.status == OrderStatus.AWAITING_PAYMENT) &&
                (order.store as Store).onlinePaymentEnabled)
              CustomButton(
                solid: false,
                color: accentColor,
                height: 6.h(),
                width: 50.w(),
                text: 'PAYMENT DETAILS',
                onTap: viewPaymentDetails,
              ),
            SizedBox(
              height: 0.5.h(),
            ),
          ],
        ),
      ),
    );
  }
}
