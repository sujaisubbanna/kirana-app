import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class CustomerSignUpScreen extends StatefulWidget {
  @override
  _CustomerSignUpScreenState createState() => _CustomerSignUpScreenState();
}

class _CustomerSignUpScreenState extends State<CustomerSignUpScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final CustomerService customerService = CustomerService();

  LocationModel location;

  void setLocation(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PlacePicker(
          apiKey: PLACES_API_KEY,
          onPlacePicked: (result) {
            final LocationModel temp = LocationModel(
              address: result.formattedAddress,
              geoLocation: GeoLocation(
                type: 'Point',
                coordinates: [
                  result.geometry.location.lng,
                  result.geometry.location.lat,
                ],
              ),
            );
            Navigator.of(context).pop();
            setState(() {
              location = temp;
            });
            _formKey.currentState.patchValue(
              {'address': location.address},
            );
            _formKey.currentState.save();
            FocusScope.of(context).unfocus();
          },
          initialPosition: LatLng(
            12.9716,
            75.7139,
          ),
          useCurrentLocation: true,
          desiredLocationAccuracy: LocationAccuracy.bestForNavigation,
          selectInitialPosition: true,
        ),
      ),
    );
  }

  void customerSignup() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final values = _formKey.currentState.value;
        await this.customerService.signUp(values['name'], location);
        hideLoading();
        Get.offAllNamed(routes.HOME);
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Create account'),
        backgroundColor: accentColor,
      ),
      body: Column(
        children: [
          Container(
            height: 70.h(),
            margin: EdgeInsets.all(
              3.w(),
            ),
            child: FormBuilder(
              key: _formKey,
              child: ListView(
                children: [
                  SizedBox(
                    height: 3.h(),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 3.5.w(),
                    ),
                    child: const Text(
                      'Add your details',
                    ),
                  ),
                  const CustomInput(
                    labelText: 'Name',
                    name: 'name',
                    requiredErrorText: 'Please enter your name',
                    keyboardType: TextInputType.name,
                  ),
                  CustomInput(
                    labelText: 'Home Location',
                    name: 'address',
                    requiredErrorText: 'Please select home location',
                    onTap: () {
                      setLocation(context);
                    },
                  ),
                  SizedBox(
                    height: 3.h(),
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          CustomButton(
            height: 7.h(),
            width: 80.w(),
            text: 'SUBMIT',
            solid: true,
            onTap: customerSignup,
          ),
          SizedBox(
            height: 2.h(),
          ),
        ],
      ),
    );
  }
}
