import 'package:after_layout/after_layout.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/customer_controller.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/widgets/customer/customer_order_card.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:toggle_bar/toggle_bar.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;

class CustomerOrdersScreen extends StatefulWidget {
  @override
  _CustomerOrdersScreenState createState() => _CustomerOrdersScreenState();
}

class _CustomerOrdersScreenState extends State<CustomerOrdersScreen>
    with AfterLayoutMixin<CustomerOrdersScreen> {
  OrderType orderType = OrderType.RECENT;
  final CustomerController customerController = Get.find<CustomerController>();
  final CustomerService customerService = CustomerService();

  @override
  void afterFirstLayout(BuildContext context) {
    if (customerController
        .orders[EnumToString.convertToString(orderType)].isEmpty) {
      getOrders();
    }
  }

  void getOrders() async {
    try {
      showLoading();
      await this.customerService.getOrders(orderType);
      hideLoading();
    } catch (e) {
      print(e);
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void setFilter(int index) {
    OrderType temp;
    switch (index) {
      case 0:
        temp = OrderType.RECENT;
        break;
      case 1:
        temp = OrderType.OPEN;
        break;
      case 2:
        temp = OrderType.COMPLETED;
        break;
      case 3:
        temp = OrderType.CANCELLED;
        break;
      default:
        temp = OrderType.OPEN;
    }
    setState(() {
      orderType = temp;
    });
    if (customerController.orders[EnumToString.convertToString(temp)].isEmpty) {
      getOrders();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
        actions: [
          SvgButton(
            color: Colors.white,
            path: 'assets/images/icons/refresh.svg',
            height: 10,
            width: 7,
            onTap: getOrders,
            edgeInsets: EdgeInsets.all(8.0),
          ),
        ],
      ),
      body: Column(
        children: [
          ToggleBar(
            backgroundColor: accentColor,
            textColor: Colors.white,
            selectedTabColor: Colors.white,
            selectedTextColor: accentColor,
            labels: [
              'Recent',
              'Open',
              'Completed',
              'Cancelled',
            ],
            onSelectionUpdated: setFilter,
          ),
          GetBuilder<CustomerController>(
            builder: (value) {
              print(orderType);
              if (value
                  .orders[EnumToString.convertToString(orderType)].isNotEmpty) {
                return Expanded(
                  child: ListView(
                    children: value
                        .orders[EnumToString.convertToString(orderType)]
                        .map((e) => CustomerOrderCard(
                              onTap: () {
                                Get.toNamed(routes.CUSTOMER_ORDER, arguments: {
                                  'order': e,
                                  'orderType': orderType,
                                });
                              },
                              order: e,
                            ))
                        .toList(),
                  ),
                );
              } else {
                return Center(
                  child: Text('No orders found'),
                );
              }
            },
          )
        ],
      ),
    );
  }
}
