import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter/services.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class CustomerStorePaymentScreen extends StatefulWidget {
  @override
  _CustomerStorePaymentScreenState createState() =>
      _CustomerStorePaymentScreenState();
}

class _CustomerStorePaymentScreenState
    extends State<CustomerStorePaymentScreen> {
  PaymentDetails paymentDetails;

  @override
  void initState() {
    paymentDetails = Get.arguments as PaymentDetails;
    super.initState();
  }

  Future<void> copy(String text) async {
    if (text.isNotEmpty) {
      await Clipboard.setData(
        ClipboardData(text: text),
      );
      return;
    } else {
      getSnackbar(title: 'Could not copy text');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Details'),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          children: [
            paymentDetails.vpa != null
                ? Container(
                    width: 95.w(),
                    padding: EdgeInsets.all(3.w()),
                    margin: EdgeInsets.all(3.w()),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        2.w(),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: lightGreyColor.withOpacity(0.5),
                          spreadRadius: 0.2,
                          blurRadius: 1,
                          offset: const Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        const Text(
                          'UPI Account',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 2.h(),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Address:',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              paymentDetails.vpa.address,
                            ),
                            IconButton(
                              icon: Icon(Icons.copy),
                              onPressed: () {
                                copy(paymentDetails.vpa.address);
                                getSnackbar(
                                  title: 'Copied to clipboard',
                                  gravity: ToastGravity.BOTTOM,
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: null,
                  ),
            paymentDetails.bankAccount != null
                ? Container(
                    width: 95.w(),
                    padding: EdgeInsets.all(3.w()),
                    margin: EdgeInsets.all(3.w()),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(
                        2.w(),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: lightGreyColor.withOpacity(0.5),
                          spreadRadius: 0.2,
                          blurRadius: 1,
                          offset: const Offset(1, 1),
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        const Text(
                          'Bank Account',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 2.h(),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Name:',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              paymentDetails.bankAccount.name,
                              textAlign: TextAlign.right,
                            ),
                            Container(
                              child: null,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'IFSC:',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              paymentDetails.bankAccount.ifsc,
                              textAlign: TextAlign.right,
                            ),
                            IconButton(
                              icon: Icon(Icons.copy),
                              onPressed: () {
                                copy(paymentDetails.bankAccount.ifsc);
                                getSnackbar(
                                  title: 'Copied to clipboard',
                                  gravity: ToastGravity.BOTTOM,
                                );
                              },
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              'Account:',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              paymentDetails.bankAccount.accountNumber,
                              textAlign: TextAlign.right,
                            ),
                            IconButton(
                              icon: Icon(Icons.copy),
                              onPressed: () {
                                copy(paymentDetails.bankAccount.accountNumber);
                                getSnackbar(
                                  title: 'Copied to clipboard',
                                  gravity: ToastGravity.BOTTOM,
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                : Container(
                    child: null,
                  ),
          ],
        ),
      ),
    );
  }
}
