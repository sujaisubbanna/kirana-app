import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/services/customer/customer_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class CustomerEditScreen extends StatefulWidget {
  @override
  _CustomerEditScreenState createState() => _CustomerEditScreenState();
}

class _CustomerEditScreenState extends State<CustomerEditScreen>
    with AfterLayoutMixin<CustomerEditScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final CustomerService customerService = CustomerService();

  LocationModel location;

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      location = Get.find<UserController>().customer.location;
    });
    _formKey.currentState.patchValue(
      {
        'address': Get.find<UserController>().customer.location.address,
        'name': Get.find<UserController>().customer.name,
      },
    );
  }

  void setLocation(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PlacePicker(
          apiKey: PLACES_API_KEY,
          onPlacePicked: (result) {
            final LocationModel temp = LocationModel(
              address: result.formattedAddress,
              geoLocation: GeoLocation(
                type: 'Point',
                coordinates: [
                  result.geometry.location.lng,
                  result.geometry.location.lat,
                ],
              ),
            );
            Navigator.of(context).pop();
            setState(() {
              location = temp;
            });
            _formKey.currentState.patchValue(
              {'address': location.address},
            );
            _formKey.currentState.save();
            FocusScope.of(context).unfocus();
          },
          initialPosition: LatLng(
            Get.find<UserController>()
                .customer
                .location
                .geoLocation
                .coordinates[1],
            Get.find<UserController>()
                .customer
                .location
                .geoLocation
                .coordinates[0],
          ),
          useCurrentLocation: false,
          desiredLocationAccuracy: LocationAccuracy.bestForNavigation,
          selectInitialPosition: true,
        ),
      ),
    );
  }

  void customerUpdate() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final values = _formKey.currentState.value;
        await this.customerService.update(values['name'], location);
        hideLoading();
        Get.offAllNamed(routes.HOME);
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Edit account'),
      ),
      body: Container(
        margin: EdgeInsets.all(
          3.w(),
        ),
        child: FormBuilder(
          key: _formKey,
          initialValue: {
            'name': Get.find<UserController>().customer.name,
            'address': Get.find<UserController>().customer.location.address,
          },
          child: Column(
            children: [
              Container(
                height: 50.h(),
                child: ListView(
                  children: [
                    SizedBox(
                      height: 3.h(),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: 3.5.w(),
                      ),
                      child: const Text(
                        'Add your details',
                      ),
                    ),
                    const CustomInput(
                      labelText: 'Name',
                      hint: 'Enter your name',
                      name: 'name',
                      requiredErrorText: 'Please enter your name',
                      keyboardType: TextInputType.name,
                    ),
                    CustomInput(
                      labelText: 'Home Location',
                      name: 'address',
                      hint: 'Malleshwaram, Bengaluru',
                      requiredErrorText: 'Please select home location',
                      onTap: () {
                        setLocation(context);
                      },
                    ),
                    SizedBox(
                      height: 3.h(),
                    ),
                  ],
                ),
              ),
              Spacer(),
              CustomButton(
                height: 7.h(),
                width: 80.w(),
                text: 'SUBMIT',
                solid: true,
                onTap: customerUpdate,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
