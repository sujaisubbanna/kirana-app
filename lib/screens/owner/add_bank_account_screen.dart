import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/models/store/payment_details_type_enum.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class BankDetailsScreen extends StatefulWidget {
  @override
  _BankDetailsScreenState createState() => _BankDetailsScreenState();
}

class _BankDetailsScreenState extends State<BankDetailsScreen> {
  final _bankDetailsFormKey = GlobalKey<FormBuilderState>();
  OwnerService ownerService = OwnerService();
  String storeId;

  @override
  void initState() {
    storeId = Get.arguments as String;
    super.initState();
  }

  void createBankAccount() async {
    if (_bankDetailsFormKey.currentState.saveAndValidate()) {
      final Map<String, dynamic> formValues =
          _bankDetailsFormKey.currentState.value;
      try {
        showLoading();
        PaymentDetails paymentDetails = PaymentDetails(
          vpa: null,
          bankAccount: BankAccount(
            accountNumber: formValues['accountNumber'],
            ifsc: formValues['ifsc'],
            name: formValues['name'],
          ),
        );
        await this.ownerService.addPaymentDetails(
              storeId,
              paymentDetails,
              PaymentDetailsTypeEnum.BANK_ACCOUNT,
            );
        hideLoading();
        Get.back();
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Please try again later');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Bank Details'),
        ),
        body: Column(
          children: [
            Container(
              height: 80.h(),
              child: FormBuilder(
                key: _bankDetailsFormKey,
                child: Container(
                  margin: EdgeInsets.all(
                    3.w(),
                  ),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 3.h(),
                      ),
                      const CustomInput(
                        labelText: 'Enter account holders name',
                        name: 'name',
                        minLength: 3,
                        maxLength: 30,
                        requiredErrorText:
                            'Please enter your Account holder name',
                      ),
                      const CustomInput(
                        labelText: 'Enter your IFSC Code',
                        name: 'ifsc',
                        requiredErrorText: 'Please enter your IFSC code',
                        ifsc: true,
                      ),
                      const CustomInput(
                        labelText: 'Enter your Account Number',
                        name: 'accountNumber',
                        requiredErrorText: 'Please enter your Account Number',
                        keyboardType: TextInputType.number,
                        minLength: 9,
                        maxLength: 18,
                        minLengthErrorText: 'Please enter valid Account Number',
                        maxLengthErrorText: 'Please enter valid Account Number',
                      ),
                    ],
                  ),
                ),
              ),
            ),
            CustomButton(
              text: 'ADD BANK ACCOUNT',
              height: 8.h(),
              width: 80.w(),
              onTap: createBankAccount,
              color: accentColor,
              solid: true,
            ),
          ],
        ),
      ),
    );
  }
}
