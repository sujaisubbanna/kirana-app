import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/shared/item_model.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/owner/order_item_edit_field.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class OrderItemsScreen extends StatefulWidget {
  @override
  _OrderItemsScreenState createState() => _OrderItemsScreenState();
}

class _OrderItemsScreenState extends State<OrderItemsScreen> {
  OwnerService ownerService = OwnerService();
  Order order;
  Store store;
  final _formKey = GlobalKey<FormBuilderState>();
  final _discountFormKey = GlobalKey<FormBuilderState>();
  List<OrderItemEditField> fields = [];
  double grossAmount = 0;
  double netAmount = 0;
  double discount = 0;

  @override
  void initState() {
    Map<String, dynamic> arguments = Get.arguments;
    order = arguments['order'] as Order;
    store = arguments['store'] as Store;
    for (var i = 0; i < order.items.length; i++) {
      fields.add(OrderItemEditField(
        formKey: _formKey,
        idx: i,
        item: order.items[i],
      ));
    }
    super.initState();
  }

  void setOrderPacked() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final List<Map<String, dynamic>> data =
            getOrderDetails(_formKey.currentState.value);
        List<Item> items = data.map((e) => Item.fromJson(e)).toList();
        if (store.paymentRequired) {
          await ownerService.acceptOrder(order.id, items, discount);
        } else {
          await ownerService.setOrderPacked(order.id, items, discount);
        }
        hideLoading();
        Get.back();
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    } else {
      getSnackbar(title: 'Check items');
    }
  }

  List<Map<String, dynamic>> getOrderDetails(Map<String, dynamic> values) {
    List<Map<String, dynamic>> temp = [];
    final length = values.length / 3;
    for (var i = 0; i < length; i++) {
      if ((values['name_$i'] as String).isNotEmpty) {
        temp.add({
          'name': values['name_$i'],
          'qty': values['qty_$i'],
          'price': double.parse(values['price_$i']),
        });
      }
    }
    return temp;
  }

  void calculateAmount() {
    _formKey.currentState.saveAndValidate();
    _discountFormKey.currentState.save();

    final List<Map<String, dynamic>> data =
        getOrderDetails(_formKey.currentState.value);
    setState(() {
      discount = double.parse(_discountFormKey.currentState.value['discount']);
      grossAmount = data.fold(
        0,
        (sum, next) => sum + next['price'],
      );
      netAmount = grossAmount - discount;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Items'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  0.5.w(),
                  5.w(),
                  0.5.w(),
                  0,
                ),
                child: Container(
                  height: 40.h(),
                  child: ListView(
                    children: fields,
                  ),
                ),
              ),
            ),
            FormBuilder(
              key: _discountFormKey,
              initialValue: {
                'discount': '0',
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                  0.5.w(),
                  5.w(),
                  0.5.w(),
                  0,
                ),
                child: CustomInput(
                  labelText: 'Discount Amount',
                  name: 'discount',
                  width: 40.w(),
                  keyboardType: TextInputType.number,
                  initialValue: '0',
                  required: false,
                  lastField: true,
                ),
              ),
            ),
            SizedBox(
              height: 2.h(),
            ),
            Container(
              color: accentColor,
              padding: EdgeInsets.symmetric(
                vertical: 4,
                horizontal: 5.w(),
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizedText(
                        text: 'Gross Amount',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      AutoSizedText(
                        text: '₹ ${grossAmount.toString()}',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerRight,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizedText(
                        text: 'Discount',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      AutoSizedText(
                        text: '₹ ${discount.toString()}',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerRight,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizedText(
                        text: 'Net Amount',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      AutoSizedText(
                        text: '₹ ${netAmount.toString()}',
                        width: 8.w(),
                        height: 0.6.h(),
                        align: Alignment.centerRight,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomButton(
                  height: 7.h(),
                  width: 45.w(),
                  color: accentColor,
                  text: 'CALCULATE AMOUNT',
                  icon: null,
                  solid: false,
                  onTap: calculateAmount,
                ),
                CustomButton(
                  height: 7.h(),
                  width: 45.w(),
                  color: accentColor,
                  text: store.paymentRequired
                      ? 'REQUEST PAYMENT'
                      : 'SET ORDER PACKED',
                  icon: null,
                  solid: false,
                  onTap: () {
                    calculateAmount();
                    Get.dialog(
                      CustomAlertDialog(
                        title: store.paymentRequired
                            ? 'Do you want to accept the order and request payment?'
                            : 'Do you want to finalize the prices and set the order to Packed?',
                        onReturn: () => Get.back(),
                        onConfirm: () {
                          Get.back();
                          setOrderPacked();
                        },
                        returnTitle: 'No',
                        confirmTitle: 'Yes',
                      ),
                    );
                  },
                ),
              ],
            ),
            SizedBox(
              height: 2.h(),
            ),
          ],
        ),
      ),
    );
  }
}
