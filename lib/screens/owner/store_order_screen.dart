import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/models/customer/customer_model.dart';
import 'package:skipyq_app/models/shared/order_status_enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/order/order_status.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class StoreOrderScreen extends StatefulWidget {
  @override
  _StoreOrderScreenState createState() => _StoreOrderScreenState();
}

class _StoreOrderScreenState extends State<StoreOrderScreen> {
  final OwnerService ownerService = OwnerService();
  String orderId;
  OrderType orderType;
  Store store;

  @override
  void initState() {
    Map<String, dynamic> arguments = Get.arguments;
    orderId = arguments['orderId'] as String;
    orderType = arguments['orderType'] as OrderType;
    store = arguments['store'] as Store;
    super.initState();
  }

  void getOrder() async {
    try {
      showLoading();
      await ownerService.getOrder(orderId, orderType);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void cancelOrder() async {
    try {
      showLoading();
      await ownerService.cancelOrder(orderId);
      setState(() {
        orderType = OrderType.CANCELLED;
      });
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void acceptOrder() async {
    try {
      showLoading();
      await ownerService.acceptOrder(orderId, null, null);
      hideLoading();
    } catch (e) {
      print(e);
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void setOrderPacked() async {
    try {
      showLoading();
      await ownerService.setOrderPacked(orderId, null, null);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void completeOrder() async {
    try {
      showLoading();
      await ownerService.completeOrder(orderId);
      setState(() {
        orderType = OrderType.COMPLETED;
      });
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Details'),
        actions: [
          SvgButton(
            color: Colors.white,
            path: 'assets/images/icons/refresh.svg',
            height: 10,
            width: 7,
            onTap: getOrder,
            edgeInsets: EdgeInsets.all(8.0),
          ),
        ],
      ),
      body: GetBuilder<OwnerController>(
        builder: (value) {
          final order =
              value.orders[EnumToString.convertToString(orderType)].firstWhere(
            (x) => x.id == orderId,
            orElse: () => null,
          );
          if (order != null) {
            return Container(
              child: Column(
                children: [
                  AutoSizedText(
                    text: '#${order.orderNo.toString()}',
                    height: 1.h(),
                    width: 12.w(),
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 3.w(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizedText(
                          text: (order.customer as Customer).name,
                          height: 1.h(),
                          width: 8.w(),
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: accentColor,
                            borderRadius: BorderRadius.circular(
                              20,
                            ),
                          ),
                          child: OrderStatusChip(
                            status: order.status,
                          ),
                        ),
                      ],
                    ),
                  ),
                  AutoSizedText(
                    text: 'Items',
                    width: 5.w(),
                    height: 1.h(),
                    align: Alignment.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: accentColor,
                    ),
                  ),
                  Container(
                    height: 40.h(),
                    padding: EdgeInsets.symmetric(
                      horizontal: 15,
                    ),
                    child: ListView(
                      children: order.items
                          .map(
                            (e) => Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizedText(
                                  text: e.name,
                                  width: 10.w(),
                                  height: 1.h(),
                                  align: Alignment.center,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                  multilineEnable: true,
                                ),
                                AutoSizedText(
                                  text: 'Qty. ${e.qty}',
                                  width: 5.w(),
                                  height: 1.h(),
                                  align: Alignment.center,
                                ),
                                if ((order.status == OrderStatus.PACKED ||
                                        order.status == OrderStatus.COMPLETED ||
                                        order.status ==
                                            OrderStatus.AWAITING_PAYMENT) &&
                                    e.price > 0)
                                  AutoSizedText(
                                    text: '₹ ${e.price}',
                                    width: 8.w(),
                                    height: 0.6.h(),
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                if ((order.status == OrderStatus.PACKED ||
                                        order.status == OrderStatus.COMPLETED ||
                                        order.status ==
                                            OrderStatus.AWAITING_PAYMENT) &&
                                    e.price == 0)
                                  AutoSizedText(
                                    text: 'Not available',
                                    width: 8.w(),
                                    height: 0.6.h(),
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ),
                  if (order.notes != null && order.notes.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                      ),
                      child: AutoSizedText(
                        text: 'NOTES: ${order.notes}',
                        height: 0.5.h(),
                        width: 80.w(),
                        multilineEnable: true,
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  if (order.status == OrderStatus.PACKED ||
                      order.status == OrderStatus.COMPLETED ||
                      order.status == OrderStatus.AWAITING_PAYMENT)
                    Container(
                      color: accentColor,
                      padding: EdgeInsets.symmetric(
                        vertical: 4,
                        horizontal: 5.w(),
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizedText(
                                text: 'Gross Amount',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerLeft,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              AutoSizedText(
                                text: '₹ ${order.grossAmount.toString()}',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerRight,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizedText(
                                text: 'Discount',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerLeft,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              AutoSizedText(
                                text: '₹ ${order.discount.toString()}',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerRight,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AutoSizedText(
                                text: 'Net Amount',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerLeft,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              AutoSizedText(
                                text: '₹ ${order.netAmount.toString()}',
                                width: 8.w(),
                                height: 0.5.h(),
                                align: Alignment.centerRight,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w800,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  Spacer(),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        if (order.status == OrderStatus.CREATED)
                          CustomButton(
                            solid: false,
                            color: Colors.green,
                            height: 7.h(),
                            width: 40.w(),
                            text: 'ACCEPT ORDER',
                            icon: Icon(
                              Icons.done,
                              color: Colors.green,
                            ),
                            onTap: () {
                              Get.dialog(
                                CustomAlertDialog(
                                  title:
                                      'Are you sure you want to accept the order?Orders once accepted cannot be cancelled.',
                                  onReturn: () => Get.back(),
                                  onConfirm: () {
                                    Get.back();
                                    if (store.paymentRequired) {
                                      Get.toNamed(routes.ORDER_ITEMS,
                                          arguments: {
                                            'order': order,
                                            'store': store
                                          });
                                    } else {
                                      acceptOrder();
                                    }
                                  },
                                  returnTitle: 'No',
                                  confirmTitle: 'Yes',
                                ),
                              );
                            },
                          ),
                        if (order.status != OrderStatus.COMPLETED &&
                            order.status != OrderStatus.CANCELLED &&
                            order.status != OrderStatus.PACKED &&
                            order.status != OrderStatus.ACCEPTED &&
                            order.status != OrderStatus.AWAITING_PAYMENT)
                          CustomButton(
                            solid: false,
                            color: Colors.red,
                            icon: Icon(
                              Icons.close,
                              color: Colors.red,
                            ),
                            height: 7.h(),
                            width: 40.w(),
                            text: 'REJECT ORDER',
                            onTap: () {
                              Get.dialog(
                                CustomAlertDialog(
                                  title: 'Do you want to cancel the order?',
                                  onReturn: () => Get.back(),
                                  onConfirm: () {
                                    Get.back();
                                    cancelOrder();
                                  },
                                  returnTitle: 'No',
                                  confirmTitle: 'Cancel',
                                ),
                              );
                            },
                          ),
                        if (order.status == OrderStatus.ACCEPTED)
                          CustomButton(
                            solid: false,
                            color: accentColor,
                            height: 7.h(),
                            width: 40.w(),
                            text: 'ENTER PRICES',
                            onTap: () {
                              Get.toNamed(routes.ORDER_ITEMS,
                                  arguments: {'order': order, 'store': store});
                            },
                          ),
                        if (order.status == OrderStatus.AWAITING_PAYMENT)
                          CustomButton(
                            solid: false,
                            color: accentColor,
                            height: 7.h(),
                            width: 40.w(),
                            text: 'SET ORDER PACKED',
                            onTap: () {
                              Get.dialog(
                                CustomAlertDialog(
                                  title:
                                      'Do you want to notify the customer to pick up the order?',
                                  onReturn: () => Get.back(),
                                  onConfirm: () {
                                    Get.back();
                                    setOrderPacked();
                                  },
                                  returnTitle: 'No',
                                  confirmTitle: 'Yes',
                                ),
                              );
                            },
                          ),
                        if (order.status == OrderStatus.PACKED)
                          CustomButton(
                            solid: true,
                            color: accentColor,
                            height: 7.h(),
                            width: 50.w(),
                            text: 'COMPLETE ORDER',
                            onTap: () {
                              Get.dialog(
                                CustomAlertDialog(
                                  title:
                                      'Do you want to mark the order as complete?',
                                  onReturn: () => Get.back(),
                                  onConfirm: () {
                                    Get.back();
                                    completeOrder();
                                  },
                                  returnTitle: 'No',
                                  confirmTitle: 'Yes',
                                ),
                              );
                            },
                          ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 0.5.h(),
                  ),
                ],
              ),
            );
          } else {
            return Container(
              width: 100.w(),
              height: 100.h(),
            );
          }
        },
      ),
    );
  }
}
