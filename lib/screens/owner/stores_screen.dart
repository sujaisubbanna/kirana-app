import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/owner/owner_drawer.dart';
import 'package:skipyq_app/widgets/owner/store_card.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_button.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class StoresScreen extends StatefulWidget {
  @override
  _StoresScreenState createState() => _StoresScreenState();
}

class _StoresScreenState extends State<StoresScreen>
    with AfterLayoutMixin<StoresScreen> {
  OwnerService ownerService = OwnerService();

  @override
  void initState() {
    bool reset = Get.arguments as bool ?? false;
    if (reset) {
      getStores();
    }
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (Get.find<OwnerController>().stores.length == 0) {
      getStores();
    }
  }

  void getStores() async {
    try {
      showLoading();
      await ownerService.getStores(null, 100);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void addStore() {
    Get.toNamed(routes.ADD_STORE);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stores'),
        backgroundColor: accentColor,
        actions: [
          SvgButton(
            color: Colors.white,
            path: 'assets/images/icons/refresh.svg',
            height: 10,
            width: 7,
            onTap: getStores,
            edgeInsets: EdgeInsets.all(8.0),
          ),
        ],
      ),
      drawer: Container(
        width: 50.w(),
        child: OwnerDrawer(),
      ),
      body: Column(
        children: [
          Container(
            height: 80.h(),
            child: GetBuilder<OwnerController>(
              builder: (value) => value.stores.length > 0
                  ? ListView(
                      children: value.stores
                          .map(
                            (e) => StoreCard(
                              store: e,
                            ),
                          )
                          .toList(),
                    )
                  : Container(
                      child: null,
                    ),
            ),
          ),
          Spacer(),
          CustomButton(
            icon: Icon(Icons.add),
            text: 'ADD STORE',
            solid: false,
            onTap: addStore,
            width: 60.w(),
            height: 5.h(),
          ),
          SizedBox(
            height: 2.h(),
          ),
        ],
      ),
    );
  }
}
