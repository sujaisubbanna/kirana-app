import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/models/store/payment_details_type_enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/owner/bank_account_card.dart';
import 'package:skipyq_app/widgets/owner/vpa_card.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class StorePaymentDetailsScreen extends StatefulWidget {
  @override
  _StorePaymentDetailsScreenState createState() =>
      _StorePaymentDetailsScreenState();
}

class _StorePaymentDetailsScreenState extends State<StorePaymentDetailsScreen> {
  final OwnerService ownerService = OwnerService();
  String storeId;

  @override
  void initState() {
    storeId = Get.arguments as String;
    super.initState();
  }

  void addBankAccount() {
    Get.toNamed(routes.ADD_BANK_ACCOUNT, arguments: storeId);
  }

  void addUPIAccount() {
    Get.toNamed(routes.ADD_UPI_ACCOUNT, arguments: storeId);
  }

  void deleteVPAAccount() async {
    try {
      showLoading();
      await this
          .ownerService
          .deletePaymentDetails(storeId, PaymentDetailsTypeEnum.VPA);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void deleteBankAccount() async {
    try {
      showLoading();
      await this
          .ownerService
          .deletePaymentDetails(storeId, PaymentDetailsTypeEnum.BANK_ACCOUNT);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Details'),
        backgroundColor: accentColor,
      ),
      body: Container(
        width: double.infinity,
        child: GetBuilder<OwnerController>(
          builder: (value) {
            Store store = value.stores.firstWhere((x) => x.id == storeId);
            return Column(
              children: [
                Container(
                  height: 40.h(),
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 10,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizedText(
                        height: 1.h(),
                        width: 20.w(),
                        text: 'Bank Account',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      store.paymentDetails?.bankAccount != null
                          ? BankAccountCard(
                              bankAccount: store.paymentDetails.bankAccount,
                              deleteAccount: deleteBankAccount,
                            )
                          : CustomButton(
                              height: 5.h(),
                              width: 60.w(),
                              text: 'ADD BANK ACCOUNT',
                              icon: Icon(
                                Icons.add,
                                color: accentColor,
                              ),
                              solid: false,
                              onTap: addBankAccount,
                              color: accentColor,
                            ),
                    ],
                  ),
                ),
                Container(
                  height: 40.h(),
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 10,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      AutoSizedText(
                        height: 1.h(),
                        width: 20.w(),
                        text: 'UPI Account',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      store.paymentDetails?.vpa != null
                          ? VPACard(
                              vpa: store.paymentDetails.vpa,
                              deleteAccount: deleteVPAAccount,
                            )
                          : CustomButton(
                              height: 5.h(),
                              width: 60.w(),
                              text: 'ADD UPI ACCOUNT',
                              icon: Icon(
                                Icons.add,
                                color: accentColor,
                              ),
                              solid: false,
                              onTap: addUPIAccount,
                              color: accentColor,
                            ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
