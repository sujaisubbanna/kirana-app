import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_dropdown.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/image_camera.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:recase/recase.dart';

class AddStoreScreen extends StatefulWidget {
  @override
  _AddStoreScreenState createState() => _AddStoreScreenState();
}

class _AddStoreScreenState extends State<AddStoreScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final UtilsService utilsService = UtilsService();
  final OwnerService ownerService = OwnerService();

  LocationModel location;
  ImageCamera imageCamera;
  String imageURL;

  @override
  void initState() {
    this.setBasicInfo();
    super.initState();
  }

  void setBasicInfo() {
    setState(() {
      imageCamera = ImageCamera(
        getImageBase64Format: setUserProfilePicture,
      );
    });
  }

  Future<void> setUserProfilePicture(
    String profilePictureBase64,
  ) async {
    showLoading();
    try {
      final String url = await utilsService.uploadImage(
        profilePictureBase64,
      );
      setState(() {
        imageURL = url;
      });
      _formKey.currentState.setInternalFieldValue('image', url);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Please try again later');
    }
  }

  void setLocation(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PlacePicker(
          apiKey: PLACES_API_KEY,
          onPlacePicked: (result) {
            final LocationModel temp = LocationModel(
              address: result.formattedAddress,
              geoLocation: GeoLocation(
                type: 'Point',
                coordinates: [
                  result.geometry.location.lng,
                  result.geometry.location.lat,
                ],
              ),
            );
            Navigator.of(context).pop();
            setState(() {
              location = temp;
            });
            _formKey.currentState.patchValue(
              {'address': location.address},
            );
            _formKey.currentState.save();
            FocusScope.of(context).unfocus();
          },
          initialPosition: LatLng(
            12.9716,
            75.7139,
          ),
          useCurrentLocation: true,
          desiredLocationAccuracy: LocationAccuracy.bestForNavigation,
          selectInitialPosition: true,
        ),
      ),
    );
  }

  void addStore() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final Map<String, dynamic> value = _formKey.currentState.value;
        await ownerService.addStore(
          name: value['name'] as String,
          location: location,
          image: value['image'] as String ?? null,
          contactNo: value['contactNo'],
          type: value['type'],
          tags: value['tags'],
        );
        await ownerService.getStores(null, 100);
        hideLoading();
        Get.back();
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    } else {
      getSnackbar(title: 'Check Owner Details');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Add Store',
        ),
      ),
      body: Column(
        children: [
          FormBuilder(
            key: _formKey,
            initialValue: {
              'name': '',
              'address': '',
              'type': StoreType.GENERAL,
              'tags': []
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                5.w(),
                1.w(),
                5.w(),
                0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 1.h(),
                  ),
                  imageURL == null
                      ? SvgContainer(
                          height: 5.w(),
                          width: 5.w(),
                          path: 'assets/images/icons/stores.svg',
                          color: accentColor,
                        )
                      : Container(
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.25),
                                spreadRadius: 0,
                                blurRadius: 4,
                                offset: const Offset(0, 4),
                              ),
                            ],
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: Image.network(
                              imageURL,
                              height: 25.w(),
                              width: 25.w(),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                  SizedBox(
                    height: 2.h(),
                  ),
                  CustomButton(
                    height: 6.h(),
                    width: 50.w(),
                    text: 'Upload Store Image',
                    icon: Icon(
                      Icons.cloud_upload_outlined,
                      color: accentColor,
                    ),
                    solid: false,
                    onTap: () {
                      imageCamera.showMyDialog();
                    },
                    color: accentColor,
                  ),
                  AutoSizedText(
                    text: 'Upload images less then 2.5Mb',
                    height: 0.5.h(),
                    width: 20.w(),
                    multilineEnable: true,
                    style: TextStyle(
                      color: accentColor,
                    ),
                  ),
                  Container(
                    height: 50.h(),
                    child: ListView(
                      children: [
                        const CustomInput(
                          minLength: 3,
                          labelText: 'Store Name',
                          maxLength: 100,
                          name: 'name',
                          requiredErrorText: 'Please enter storename',
                          text: true,
                        ),
                        const CustomInput(
                          minLength: 10,
                          labelText: 'Store Number',
                          maxLength: 10,
                          name: 'contactNo',
                          hint: '123456789',
                          requiredErrorText: 'Please enter store number',
                          keyboardType: TextInputType.phone,
                        ),
                        CustomInput(
                          labelText: 'Store Address',
                          name: 'address',
                          requiredErrorText: 'Please select the store location',
                          onTap: () {
                            setLocation(context);
                          },
                        ),
                        CustomDropdown(
                          name: 'type',
                          onChanged: (value) {
                            _formKey.currentState
                                .setInternalFieldValue('type', value);
                            _formKey.currentState.save();
                            setState(() {});
                          },
                          initialValue: StoreType.GENERAL,
                          requiredErrorText: 'Pick a store type',
                          items: StoreType.values.map((e) {
                            return DropdownMenuItem<StoreType>(
                              value: e,
                              child: Text(
                                EnumToString.convertToString(e),
                              ),
                            );
                          }).toList(),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: accentColor),
                            borderRadius: BorderRadius.circular(
                              14,
                            ),
                            color: Colors.white,
                          ),
                          height: 18.h(),
                          margin: EdgeInsets.symmetric(
                            horizontal: 5.w(),
                          ),
                          child: ListView(
                            children: [
                              FormBuilderFilterChip(
                                backgroundColor: Colors.white,
                                clipBehavior: Clip.none,
                                selectedColor:
                                    Color.fromARGB(255, 228, 255, 225),
                                labelStyle: TextStyle(
                                  color: accentColor,
                                ),
                                checkmarkColor: accentColor,
                                spacing: 5,
                                alignment: WrapAlignment.center,
                                // validator: (value) {
                                //   if (value.length == 0) {
                                //     return 'Select at least one tag';
                                //   }
                                //   return null;
                                // },
                                name: 'tags',
                                options: utilsService
                                    .getStoreTags(
                                      _formKey.currentState?.fields
                                                  ?.containsKey('type') ??
                                              false
                                          ? _formKey
                                              .currentState.fields['type'].value
                                          : null,
                                    )
                                    .map(
                                      (e) => FormBuilderFieldOption(
                                        value: e,
                                        child: Text(
                                          EnumToString.convertToString(e)
                                              .titleCase,
                                        ),
                                      ),
                                    )
                                    .toList(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          CustomButton(
            onTap: addStore,
            height: 7.h(),
            width: 80.w(),
            text: 'ADD STORE',
            solid: true,
          ),
          SizedBox(
            height: 1.h(),
          ),
        ],
      ),
    );
  }
}
