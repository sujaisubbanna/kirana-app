import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/models/store/payment_details_type_enum.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';

class UPIDetailsScreen extends StatefulWidget {
  @override
  _UPIDetailsScreenState createState() => _UPIDetailsScreenState();
}

class _UPIDetailsScreenState extends State<UPIDetailsScreen> {
  final _upiFormKey = GlobalKey<FormBuilderState>();

  OwnerService ownerService = OwnerService();
  String storeId;

  @override
  void initState() {
    storeId = Get.arguments as String;
    super.initState();
  }

  void createVPAAccount() async {
    if (_upiFormKey.currentState.saveAndValidate()) {
      showLoading();
      try {
        PaymentDetails paymentDetails = PaymentDetails(
          bankAccount: null,
          vpa: VPA(
            address: _upiFormKey.currentState.value['upi'],
          ),
        );
        await this.ownerService.addPaymentDetails(
              storeId,
              paymentDetails,
              PaymentDetailsTypeEnum.VPA,
            );
        hideLoading();
        Get.back();
      } catch (e) {
        hideLoading();
        getSnackbar(title: 'Please try again later');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('UPI Details'),
        ),
        body: Container(
          margin: EdgeInsets.all(
            3.w(),
          ),
          child: Column(
            children: [
              Container(
                height: 75.h(),
                child: FormBuilder(
                  key: _upiFormKey,
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 3.h(),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          left: 3.5.w(),
                        ),
                        child: const Text(
                          'Add your VPA (Virtual Private Address) / UPI',
                        ),
                      ),
                      const CustomInput(
                        hint: 'Enter your UPI Code',
                        name: 'upi',
                        upi: true,
                        requiredErrorText: 'Please enter your UPI code',
                        keyboardType: TextInputType.emailAddress,
                      ),
                      SizedBox(
                        height: 3.h(),
                      ),
                    ],
                  ),
                ),
              ),
              CustomButton(
                text: 'ADD UPI ACCOUNT',
                height: 8.h(),
                width: 80.w(),
                onTap: createVPAAccount,
                color: accentColor,
                solid: true,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
