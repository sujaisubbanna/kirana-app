import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/services/utils/utils_service.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/svg_container.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:recase/recase.dart';

class ManageStoreScreen extends StatefulWidget {
  @override
  _ManageStoreScreenState createState() => _ManageStoreScreenState();
}

class _ManageStoreScreenState extends State<ManageStoreScreen> {
  String storeId;
  OwnerService ownerService = OwnerService();
  UtilsService utilsService = UtilsService();

  @override
  void initState() {
    storeId = Get.arguments as String;
    super.initState();
  }

  void editStore() {
    Store store =
        Get.find<OwnerController>().stores.firstWhere((x) => x.id == storeId);
    Get.toNamed(routes.EDIT_STORE, arguments: store);
  }

  void storePaymentDetails(Store store) {
    Get.toNamed(routes.STORE_PAYMENT_DETAILS, arguments: store.id);
  }

  void toggleStoreOpenStatus() async {
    try {
      showLoading();
      await this.ownerService.toggleOpenStatus(storeId);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void toggleOnlinePaymentEnabled() async {
    try {
      showLoading();
      await this.ownerService.toggleOnlinePaymentEnabled(storeId);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void togglePaymentRequired() async {
    try {
      showLoading();
      await this.ownerService.togglePaymentRequired(storeId);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Manage Store'),
      ),
      body: GetBuilder<OwnerController>(
        builder: (value) {
          Store store = value.stores.firstWhere((x) => x.id == storeId);
          return Container(
            width: double.infinity,
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  height: 45.h(),
                  child: Column(
                    children: [
                      store.image == null
                          ? SvgContainer(
                              height: 3.w(),
                              width: 3.w(),
                              path: 'assets/images/icons/stores.svg',
                              color: accentColor,
                            )
                          : Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.25),
                                    spreadRadius: 0,
                                    blurRadius: 4,
                                    offset: const Offset(0, 4),
                                  ),
                                ],
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: Image.network(
                                  store.image,
                                  height: 25.w(),
                                  width: 25.w(),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                      AutoSizedText(
                        text: store.name,
                        height: 1.h(),
                        width: 80.w(),
                        style: TextStyle(
                          fontSize: 28,
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: store.tags
                              .map(
                                (e) => Container(
                                  margin: EdgeInsets.symmetric(
                                    horizontal: 1.w(),
                                  ),
                                  child: Chip(
                                    backgroundColor: Colors.white,
                                    labelStyle: TextStyle(
                                      color: accentColor,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 12,
                                    ),
                                    labelPadding: EdgeInsets.symmetric(
                                      horizontal: 4,
                                      vertical: 0,
                                    ),
                                    label: Text(
                                      EnumToString.convertToString(e).titleCase,
                                    ),
                                    shape: StadiumBorder(
                                      side: BorderSide(
                                        color: accentColor,
                                        width: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          utilsService.getStoreIcon(store.type),
                          AutoSizedText(
                            align: Alignment.center,
                            text: EnumToString.convertToString(store.type),
                            height: 0.6.h(),
                            width: 6.w(),
                            multilineEnable: true,
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                      AutoSizedText(
                        align: Alignment.center,
                        text: store.location.address,
                        height: 0.8.h(),
                        width: 60.w(),
                        multilineEnable: true,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: 1.h(),
                      ),
                      CustomButton(
                        height: 6.h(),
                        width: 40.w(),
                        text: 'Edit Store',
                        icon: Icon(
                          Icons.edit_outlined,
                          color: accentColor,
                        ),
                        onTap: editStore,
                        solid: false,
                        color: accentColor,
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizedText(
                          text: 'Store open',
                          width: 6.w(),
                          height: 0.5.h(),
                          align: Alignment.centerLeft,
                          style: TextStyle(
                            fontSize: 24,
                          ),
                        ),
                        Switch(
                          value: store.open,
                          onChanged: (_) {
                            toggleStoreOpenStatus();
                          },
                          activeTrackColor: accentColor,
                          activeColor: Colors.white,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizedText(
                          text: 'Accept Online Payments',
                          width: 13.w(),
                          height: 0.5.h(),
                          align: Alignment.centerLeft,
                          style: TextStyle(
                            fontSize: 24,
                          ),
                        ),
                        Switch(
                          value: store.onlinePaymentEnabled,
                          onChanged: (_) {
                            toggleOnlinePaymentEnabled();
                          },
                          activeTrackColor: accentColor,
                          activeColor: Colors.white,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AutoSizedText(
                          text: 'Pre-Payment required for order',
                          width: 15.w(),
                          height: 0.5.h(),
                          align: Alignment.centerLeft,
                          style: TextStyle(
                            fontSize: 24,
                          ),
                        ),
                        Switch(
                          value: store.paymentRequired,
                          onChanged: (_) {
                            togglePaymentRequired();
                          },
                          activeTrackColor: accentColor,
                          activeColor: Colors.white,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15.h(),
                    ),
                    store.onlinePaymentEnabled
                        ? store.paymentDetails == null
                            ? CustomButton(
                                height: 8.h(),
                                width: 85.w(),
                                text: 'ADD PAYMENT DETAILS',
                                solid: true,
                                onTap: () {
                                  storePaymentDetails(store);
                                },
                                color: accentColor,
                              )
                            : CustomButton(
                                height: 6.h(),
                                width: 85.w(),
                                text: 'VIEW PAYMENT DETAILS',
                                solid: false,
                                onTap: () {
                                  storePaymentDetails(store);
                                },
                                color: accentColor,
                              )
                        : Container(
                            child: null,
                          ),
                    SizedBox(
                      height: 1.h(),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
