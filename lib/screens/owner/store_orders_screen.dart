import 'package:after_layout/after_layout.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';
import 'package:skipyq_app/services/owner/owner_service.dart';
import 'package:skipyq_app/widgets/owner/store_order_card.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:toggle_bar/toggle_bar.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;

class StoreOrdersScreen extends StatefulWidget {
  @override
  _StoreOrdersScreenState createState() => _StoreOrdersScreenState();
}

class _StoreOrdersScreenState extends State<StoreOrdersScreen>
    with AfterLayoutMixin<StoreOrdersScreen> {
  Store store;
  OrderType orderType = OrderType.OPEN;
  final OwnerService ownerService = OwnerService();

  @override
  void initState() {
    store = Get.arguments as Store;
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    if (getStoreOrders(Get.find<OwnerController>().orders).isEmpty) {
      getOrders();
    }
  }

  void getOrders() async {
    try {
      showLoading();
      List<Order> temp =
          await this.ownerService.getStoreOrders(store.id, orderType);
      Get.find<OwnerController>().setOrders(temp, orderType);
      hideLoading();
    } catch (_) {
      hideLoading();
      getSnackbar(title: 'Try again later');
    }
  }

  void setFilter(int index) {
    OrderType temp;
    switch (index) {
      case 0:
        temp = OrderType.OPEN;
        break;
      case 1:
        temp = OrderType.COMPLETED;
        break;
      case 2:
        temp = OrderType.CANCELLED;
        break;
      default:
        temp = OrderType.OPEN;
    }
    setState(() {
      orderType = temp;
    });
    if (getStoreOrders(Get.find<OwnerController>().orders).isEmpty) {
      getOrders();
    }
  }

  List<Order> getStoreOrders(Map<String, List<Order>> orders) {
    return orders[EnumToString.convertToString(orderType)]
        .where((x) => (x.store as String) == store.id)
        .toList();
  }

  void viewOrder(Order order) {
    Get.toNamed(routes.STORE_ORDER, arguments: {
      'orderId': order.id,
      'orderType': orderType,
      'store': store,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
        actions: [
          IconButton(
            icon: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
            onPressed: getOrders,
          )
        ],
      ),
      body: Column(
        children: [
          ToggleBar(
            backgroundColor: Colors.white,
            textColor: accentColor,
            selectedTabColor: accentColor,
            selectedTextColor: Colors.white,
            labels: [
              'Open',
              'Completed',
              'Cancelled',
            ],
            onSelectionUpdated: setFilter,
          ),
          GetBuilder<OwnerController>(builder: (value) {
            return getStoreOrders(value.orders).isNotEmpty
                ? Expanded(
                    child: ListView(
                      children: getStoreOrders(value.orders)
                          .map(
                            (e) => StoreOrderCard(
                              store: e,
                              onTap: () {
                                viewOrder(e);
                              },
                            ),
                          )
                          .toList(),
                    ),
                  )
                : Center(
                    child: Text('No orders found'),
                  );
          }),
        ],
      ),
    );
  }
}
