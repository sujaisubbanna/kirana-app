import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/config/size_config.dart';
import 'package:skipyq_app/widgets/shared/dialogs/close_app.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';

class RoleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: onCloseApp,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFFFFFFFF),
                    Color(0xFFD3EAFF),
                  ],
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(230, -40),
              child: Transform.rotate(
                angle: math.pi / 9,
                child: Container(
                  height: 100.h(),
                  width: 15.w(),
                  color: Color(0xFFD1ECFF),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(350, -40),
              child: Transform.rotate(
                angle: math.pi / 9,
                child: Container(
                  height: 100.h(),
                  width: 15.w(),
                  color: Color(0xFFD1ECFF),
                ),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 40, 0, 0),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/logos/logo_dark.png',
                            height: 8.h(),
                          ),
                          SizedBox(
                            width: 5.w(),
                          ),
                          Text(
                            'SkipyQ',
                            style: TextStyle(
                              fontSize: 28,
                              color: accentColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.h(),
                ),
                Card(
                  child: Container(
                    width: 90.w(),
                    height: 20.h(),
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        AutoSizedText(
                          text: 'I am a CUSTOMER',
                          width: 20.w(),
                          height: 0.5.h(),
                          textAlign: TextAlign.left,
                          multilineEnable: false,
                          align: Alignment.center,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        CustomButton(
                          solid: true,
                          text: 'LOGIN/SIGN UP',
                          onTap: () {
                            Get.offAllNamed(routes.LOGIN);
                          },
                          height: 5.h(),
                          width: 60.w(),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.h(),
                ),
                Card(
                  child: Container(
                    width: 90.w(),
                    height: 20.h(),
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        AutoSizedText(
                          text: 'I am a STORE OWNER',
                          width: 20.w(),
                          height: 0.5.h(),
                          textAlign: TextAlign.left,
                          multilineEnable: false,
                          align: Alignment.center,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            CustomButton(
                              solid: true,
                              text: 'LOGIN',
                              onTap: () {
                                Get.offAllNamed(routes.LOGIN);
                              },
                              height: 5.h(),
                              width: 35.w(),
                            ),
                            CustomButton(
                              solid: true,
                              text: 'SIGN UP',
                              onTap: () {
                                Get.offAllNamed(routes.OWNER_SIGN_UP);
                              },
                              height: 5.h(),
                              width: 35.w(),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

TextStyle phoneNumberText = TextStyle(
  fontSize: 5.w(),
  fontWeight: FontWeight.w300,
);

TextStyle otpText = TextStyle(
  fontSize: 5.w(),
  fontWeight: FontWeight.w700,
  color: Colors.white,
);

BoxDecoration containerDecoration(Color color) {
  return BoxDecoration(
    color: color,
    borderRadius: BorderRadius.circular(
      9,
    ),
    border: Border.all(
      color: Color(0xFF583D72),
      width: 0.1,
    ),
    boxShadow: [
      BoxShadow(
        color: lightGreyColor.withOpacity(0.3),
        spreadRadius: 1,
        blurRadius: 1,
        offset: const Offset(1, 1),
      ),
    ],
  );
}
