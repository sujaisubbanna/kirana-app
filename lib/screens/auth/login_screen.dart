import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/config/size_config.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/services/auth/auth_service.dart';
import 'package:skipyq_app/widgets/shared/dialogs/close_app.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:math' as math;

class LoginScreen extends StatelessWidget {
  final phoneNumberController = TextEditingController();
  final FocusNode _nodeText1 = FocusNode();

  Future<void> onLogin() async {
    final AuthService authService = AuthService();
    final RegExp exp = RegExp(r'(\d{10})');
    if (!exp.hasMatch(phoneNumberController.text)) {
      getSnackbar(
        title: 'Please enter a valid phone number',
      );
    } else {
      try {
        showLoading();
        final String mobile = phoneNumberController.text;
        await authService.sendConfirmationCode(
          mobile: mobile,
        );
        hideLoading();
        Get.toNamed(
          routes.OTP,
          arguments: mobile,
        );
      } catch (_) {
        hideLoading();
        getSnackbar(
          title: 'Try again later',
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: onCloseApp,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFFFFFFFF),
                    Color(0xFFD3EAFF),
                  ],
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(230, -40),
              child: Transform.rotate(
                angle: math.pi / 9,
                child: Container(
                  height: 100.h(),
                  width: 15.w(),
                  color: Color(0xFFD1ECFF),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(350, -40),
              child: Transform.rotate(
                angle: math.pi / 9,
                child: Container(
                  height: 100.h(),
                  width: 15.w(),
                  color: Color(0xFFD1ECFF),
                ),
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 40, 0, 0),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/logos/logo_dark.png',
                            height: 8.h(),
                          ),
                          SizedBox(
                            width: 5.w(),
                          ),
                          Text(
                            'SkipyQ',
                            style: TextStyle(
                              fontSize: 28,
                              color: accentColor,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.h(),
                ),
                Container(
                  width: 85.w(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizedText(
                        text: 'ENTER PHONE NUMBER',
                        width: 20.w(),
                        height: 0.5.h(),
                        textAlign: TextAlign.left,
                        multilineEnable: false,
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      AutoSizedText(
                        text: 'We will send a 4 digit code to this number',
                        width: 20.w(),
                        height: 1.h(),
                        multilineEnable: true,
                        textAlign: TextAlign.left,
                        align: Alignment.centerLeft,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: 7.h(),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 15.w(),
                              height: 8.h(),
                              decoration: containerDecoration(Colors.white),
                              child: Center(
                                child: Text(
                                  '+91',
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: 65.w(),
                              height: 8.h(),
                              decoration: containerDecoration(Colors.white),
                              padding: EdgeInsets.symmetric(
                                horizontal: 4.w(),
                              ),
                              child: TextField(
                                // textAlignVertical: TextAlignVertical.center,
                                onEditingComplete: onLogin,
                                focusNode: _nodeText1,
                                style: phoneNumberText,
                                controller: phoneNumberController,
                                maxLength: 10,
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.go,
                                textAlignVertical: TextAlignVertical.center,
                                decoration: InputDecoration(
                                  // contentPadding: EdgeInsets.zero,
                                  counterText: '',
                                  prefixStyle: phoneNumberText,
                                  hintText: 'Enter Phone number',
                                  hintStyle: phoneNumberText,
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 35.h(),
                      ),
                      CustomButton(
                        height: 8.h(),
                        width: 85.w(),
                        text: 'GET OTP',
                        icon: null,
                        solid: true,
                        onTap: onLogin,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

TextStyle phoneNumberText = TextStyle(
  fontSize: 5.w(),
  fontWeight: FontWeight.w300,
);

TextStyle otpText = TextStyle(
  fontSize: 5.w(),
  fontWeight: FontWeight.w700,
  color: Colors.white,
);

BoxDecoration containerDecoration(Color color) {
  return BoxDecoration(
    color: color,
    borderRadius: BorderRadius.circular(
      9,
    ),
    border: Border.all(
      color: Color(0xFF583D72),
      width: 0.1,
    ),
    boxShadow: [
      BoxShadow(
        color: lightGreyColor.withOpacity(0.3),
        spreadRadius: 1,
        blurRadius: 1,
        offset: const Offset(1, 1),
      ),
    ],
  );
}
