import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/constants.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/services/auth/auth_service.dart';
import 'package:skipyq_app/widgets/auth/otp_resend.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';
import 'package:skipyq_app/widgets/shared/layout/auto_sized_text.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/layout/pin_field_autofills.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'dart:math' as math;

class OTPScreen extends StatefulWidget {
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  TextEditingController otpController = TextEditingController();
  int otpVerifyCount = 0;
  final FocusNode _nodeText1 = FocusNode();
  String mobile;

  @override
  void initState() {
    mobile = Get.arguments as String;
    _nodeText1.requestFocus();
    listenForOTP();
    super.initState();
  }

  void listenForOTP() async {
    await SmsAutoFill().listenForCode;
  }

  @override
  void dispose() {
    _nodeText1.dispose();
    SmsAutoFill().unregisterListener();
    super.dispose();
  }

  void verifyOTP(String enteredOTP) async {
    final AuthService authService = AuthService();
    try {
      showLoading();
      final Map<String, dynamic> response =
          await authService.verifyConfirmationCode(
        confirmationCode: enteredOTP,
        mobile: mobile,
      );
      Get.find<AppController>().setAuthToken(response['token']);
      final result = authService.setUser(response['data'], response['token']);
      hideLoading();
      Get.offAllNamed(result['route'] as String, arguments: result['data']);
    } catch (_) {
      hideLoading();
      getSnackbar(
        title: 'Please try again later',
      );
    } finally {
      SmsAutoFill().unregisterListener();
    }
  }

  void onCodeSubmitted(String otpDetectedValue) {
    if (otpVerifyCount < OTP_VERIFICATION_LIMIT) {
      if (otpDetectedValue.length == OTP_CODE_LENGTH) {
        setState(() {
          otpVerifyCount += 1;
        });
        verifyOTP(otpDetectedValue);
      }
    } else {
      Get.dialog(
        CustomAlertDialog(
          title: 'Exceeded the number of OTP attempts',
          confirmTitle: 'Retry',
          onConfirm: () {
            if (Get.isDialogOpen) {
              Get.close(2);
            }
          },
        ),
      );
    }
  }

  void resetOTP() {
    otpController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFFFFFFF),
                  Color(0xFFD3EAFF),
                ],
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(200, -40),
            child: Transform.rotate(
              angle: math.pi / 9,
              child: Container(
                height: 100.h(),
                width: 20.w(),
                color: Color(0xFFD1ECFF),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(350, -40),
            child: Transform.rotate(
              angle: math.pi / 9,
              child: Container(
                height: 100.h(),
                width: 20.w(),
                color: Color(0xFFD1ECFF),
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 40, 0, 0),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/logos/logo_dark.png',
                          height: 8.h(),
                        ),
                        SizedBox(
                          width: 5.w(),
                        ),
                        Text(
                          'SkipyQ',
                          style: TextStyle(
                            fontSize: 28,
                            color: accentColor,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 15.h(),
              ),
              Container(
                width: 85.w(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizedText(
                      text: 'ENTER OTP',
                      width: 20.w(),
                      height: 0.5.h(),
                      textAlign: TextAlign.left,
                      align: Alignment.centerLeft,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    AutoSizedText(
                      text: 'We have sent a 4 digit code to this +91 $mobile',
                      width: 20.w(),
                      height: 1.h(),
                      multilineEnable: true,
                      textAlign: TextAlign.left,
                      align: Alignment.centerLeft,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: 5.h(),
                    ),
                    Column(
                      children: [
                        Container(
                          width: 85.w(),
                          height: 10.h(),
                          child: PinFieldAutoFills(
                            focusNode: _nodeText1,
                            controller: otpController,
                            codeLength: OTP_CODE_LENGTH,
                            decoration: BoxLooseDecoration(
                              textStyle: bottomSheetText,
                              strokeWidth: 0.3,
                              strokeColorBuilder: const FixedColorBuilder(
                                Color(0xFF583D72),
                              ),
                              bgColorBuilder: const FixedColorBuilder(
                                Colors.white,
                              ),
                            ),
                            onCodeChanged: (otpDetectedValue) async {
                              if (otpDetectedValue.length == OTP_CODE_LENGTH) {
                                onCodeSubmitted(otpDetectedValue);
                              }
                            },
                            onCodeSubmitted: (otpDetectedValue) async {
                              if (otpDetectedValue.length == OTP_CODE_LENGTH) {
                                onCodeSubmitted(otpDetectedValue);
                              }
                            },
                          ),
                        ),
                        Container(
                          height: 16.h(),
                          child: OTPResend(
                            mobile: mobile,
                            resetOTP: resetOTP,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

TextStyle bottomSheetText = TextStyle(
  fontSize: 5.w(),
  color: primaryColor,
  fontFamily: 'Metropolis',
);
