import 'package:after_layout/after_layout.dart';
import 'package:get/instance_manager.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/config/size_config.dart';
import 'package:skipyq_app/config/size_config_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/services/auth/auth_service.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with AfterLayoutMixin<SplashScreen> {
  AuthService authService = new AuthService();

  @override
  void afterFirstLayout(BuildContext context) {
    navigate();
  }

  void navigate() async {
    String token = Get.find<AppController>().authToken;
    if (token == null) {
      Get.offAllNamed(routes.ROLE_SELECT);
    } else {
      try {
        Map<String, dynamic> response = await authService.whoAmI(token);
        Get.offAllNamed(response['route'] as String,
            arguments: response['data']);
      } catch (_) {
        Get.find<AppController>().logout();
        Get.offAllNamed(routes.ROLE_SELECT);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: accentColor,
      body: Center(
        child: Container(
          width: 80.w(),
          child: Image.asset(
            'assets/images/logos/splash_icon.png',
          ),
        ),
      ),
    );
  }
}
