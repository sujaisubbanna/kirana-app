import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:skipyq_app/config/colors.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/services/auth/auth_service.dart';
import 'package:skipyq_app/widgets/shared/dialogs/alert_dialog.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_button.dart';
import 'package:skipyq_app/widgets/shared/layout/custom_input.dart';
import 'package:skipyq_app/widgets/shared/layout/loading_indicators.dart';
import 'package:skipyq_app/widgets/shared/snackbar.dart';
import 'package:skipyq_app/config/size_config_extension.dart';

class OwnerSignUpScreen extends StatefulWidget {
  @override
  _OwnerSignUpScreenState createState() => _OwnerSignUpScreenState();
}

class _OwnerSignUpScreenState extends State<OwnerSignUpScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final AuthService authService = AuthService();

  void ownerSignUp() async {
    if (_formKey.currentState.saveAndValidate()) {
      try {
        showLoading();
        final values = _formKey.currentState.value;
        await this.authService.ownerSignUp(values['name'], values['mobile']);
        hideLoading();
        Get.dialog(
          CustomAlertDialog(
            title: 'We will reach out to you within the next 48 hrs.',
            onConfirm: () {
              Get.offAllNamed(routes.ROLE_SELECT);
            },
            confirmTitle: 'OK',
          ),
        );
      } catch (_) {
        hideLoading();
        getSnackbar(title: 'Try again later');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Sign up'),
        backgroundColor: accentColor,
      ),
      body: Column(
        children: [
          Container(
            height: 70.h(),
            margin: EdgeInsets.all(
              3.w(),
            ),
            child: FormBuilder(
              key: _formKey,
              child: ListView(
                children: [
                  SizedBox(
                    height: 3.h(),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 3.5.w(),
                    ),
                    child: const Text(
                      'Add your details',
                    ),
                  ),
                  const CustomInput(
                    labelText: 'Name',
                    name: 'name',
                    requiredErrorText: 'Please enter your name',
                    keyboardType: TextInputType.name,
                  ),
                  CustomInput(
                    labelText: 'Mobile',
                    name: 'mobile',
                    requiredErrorText: 'Please enter mobile number',
                    keyboardType: TextInputType.number,
                    maxLength: 10,
                    minLength: 10,
                    minLengthErrorText: 'Please enter valid number',
                    maxLengthErrorText: 'Please enter valid number',
                    lastField: true,
                  ),
                  SizedBox(
                    height: 3.h(),
                  ),
                ],
              ),
            ),
          ),
          Spacer(),
          CustomButton(
            height: 7.h(),
            width: 80.w(),
            text: 'SUBMIT',
            solid: true,
            onTap: ownerSignUp,
          ),
          SizedBox(
            height: 2.h(),
          ),
        ],
      ),
    );
  }
}
