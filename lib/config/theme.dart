import 'package:skipyq_app/config/colors.dart';
import 'package:flutter/material.dart';

ThemeData theme = ThemeData(
  primaryColor: accentColor,
  fontFamily: 'Sen',
  buttonTheme: ButtonThemeData(
    buttonColor: accentColor,
    textTheme: ButtonTextTheme.primary,
  ),
);
