import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF131200);
const Color accentColor = Color(0xFF0496FF);
const Color lightColor = Color(0xFFF2F8F4);
const Color lightGreyColor = Color(0xFF9F9F9F);
