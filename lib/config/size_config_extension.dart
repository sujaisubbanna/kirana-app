import 'package:skipyq_app/config/size_config.dart';

extension Responsive on num {
  double w() {
    return (this * SizeConfig.safeBlockHorizontal).toDouble();
  }

  double h() {
    return (this * SizeConfig.safeBlockVertical).toDouble();
  }
}
