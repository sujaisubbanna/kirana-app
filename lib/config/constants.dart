import 'package:flutter_dotenv/flutter_dotenv.dart';

const String PLACES_API_KEY = 'AIzaSyDH74Y1VreZW-3dO5jM-Tu1jW_rDtTI7HI';
const int OTP_AWAIT_DURATION = 30;
const int OTP_CODE_LENGTH = 4;
const int OTP_VERIFICATION_LIMIT = 2;
const int MAX_IMAGE_FILE_SIZE = 2500000;
const String DISCORD_WEBHOOK_URL =
    'https://discord.com/api/webhooks/851406589138042910/wE0daIoyemnms-PYwMcQij-HBFkjN_87QmGNvZEfjz6x0YXoHItU8ozRNvwoP6iVYCVS';

String getEndpoint() {
  var env = dotenv.env['ENV'];
  if (env == 'local') {
    return 'http://1098bbd41530.ngrok.io';
  } else if (env == 'prod') {
    return 'https://api.skipyq.com';
  }
  return 'http://localhost:3000';
}

String sendCodeEndpoint(String url) {
  var env = dotenv.env['ENV'];
  if (env == 'prod') {
    return '$url/auth';
  } else {
    return '$url/auth/send';
  }
}

String verifyCodeEndpoint(String url) {
  var env = dotenv.env['ENV'];
  if (env == 'prod') {
    return '$url/auth';
  } else {
    return '$url/auth/verify';
  }
}
