import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/services/auth/auth_service.dart';
import 'package:skipyq_app/services/utils/notification_service.dart';

import '../main.dart';

class FirebaseNotifications {
  FirebaseMessaging _firebaseMessaging;
  AuthService _authService = AuthService();

  void getToken({String existingToken}) async {
    if (_firebaseMessaging == null) {
      await setUpFirebase();
    }
    String token = await _firebaseMessaging.getToken();
    if (existingToken == null || existingToken != token) {
      Get.find<AppController>().pushToken = token;
      _authService.updatePushToken();
    }
  }

  Future<void> setUpFirebase() async {
    try {
      await Firebase.initializeApp();
      _firebaseMessaging = FirebaseMessaging.instance;
      _fcmListeners();
    } catch (_) {}
  }

  void _fcmListeners() {
    if (Platform.isIOS) _iOSPermission();

    //Handle notification when app is running on background
    FirebaseMessaging.onMessageOpenedApp.listen(
      (RemoteMessage message) {
        print('onMessageOpenedApp: ${message.data}');
      },
    );

    FirebaseMessaging.onBackgroundMessage(
      firebaseMessagingBackgroundHandler,
    );

    // Handle foreground notifications, add `flutter_local_notification` to show notification
    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) {
        NotificationService().showNotification(
          message.notification.title,
          message.notification.body,
          message.data.toString(),
        );
        return;
      },
    );
  }

  Future<void> _iOSPermission() async {
    await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }
}
