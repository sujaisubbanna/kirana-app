import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';

class OwnerController extends GetxController {
  List<Store> stores = [];
  Map<String, List<Order>> orders = {
    EnumToString.convertToString(OrderType.OPEN): [],
    EnumToString.convertToString(OrderType.COMPLETED): [],
    EnumToString.convertToString(OrderType.CANCELLED): [],
  };

  void setStore(String id, Store store) {
    stores[stores.indexWhere((x) => x.id == id)] = store;
    update();
  }

  void setStores(List<Store> data) {
    stores = data;
    update();
  }

  void setOrder(Order temp, OrderType orderType) {
    final idx = orders[EnumToString.convertToString(orderType)]
        .indexWhere((x) => x.id == temp.id);
    orders[EnumToString.convertToString(orderType)][idx] = temp;
    update();
  }

  void removeOrder(Order order, OrderType orderType, OrderType target) {
    orders[EnumToString.convertToString(orderType)]
        .removeWhere((x) => x.id == order.id);
    orders[EnumToString.convertToString(target)].add(order);
    update();
  }

  void setOrders(List<Order> temp, OrderType orderType) {
    orders[EnumToString.convertToString(orderType)] = temp;
    update();
  }

  void clear() {
    stores = [];
    orders = {
      EnumToString.convertToString(OrderType.OPEN): [],
      EnumToString.convertToString(OrderType.COMPLETED): [],
      EnumToString.convertToString(OrderType.CANCELLED): [],
    };
  }
}
