import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:skipyq_app/models/owner/owner_model.dart';

class AdminController extends GetxController {
  List<Owner> owners = [];

  void setOwners(List<Owner> temp) {
    owners = temp;
    update();
  }

  void clear() {
    owners = [];
  }
}
