import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:skipyq_app/models/admin/admin_model.dart';
import 'package:skipyq_app/models/customer/customer_model.dart';
import 'package:skipyq_app/models/owner/owner_model.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';

class UserController extends GetxController {
  Admin admin;
  Owner owner;
  Customer customer;

  void setUser(dynamic user, Roles role) {
    switch (role) {
      case Roles.ADMIN:
        admin = Admin.fromJson(user);
        break;
      case Roles.CUSTOMER:
        customer = Customer.fromJson(user);
        break;
      case Roles.OWNER:
        owner = Owner.fromJson(user);
        break;
    }
  }

  void setOrders(List<Order> orders, OrderType orderType) {
    customer.orders[EnumToString.convertToString(orderType)] =
        orders.map((e) => e.id).toList();
    update();
  }

  void clear() {
    admin = null;
    customer = null;
    owner = null;
  }
}
