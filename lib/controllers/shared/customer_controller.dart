import 'package:enum_to_string/enum_to_string.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:skipyq_app/models/shared/order_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';

class CustomerController extends GetxController {
  List<Store> stores = [];
  Map<String, List<Order>> orders = {
    EnumToString.convertToString(OrderType.RECENT): [],
    EnumToString.convertToString(OrderType.OPEN): [],
    EnumToString.convertToString(OrderType.COMPLETED): [],
    EnumToString.convertToString(OrderType.CANCELLED): [],
  };

  void setStores(List<Store> temp) {
    stores = temp;
    update();
  }

  void clear() {
    stores = [];
    orders = {
      EnumToString.convertToString(OrderType.RECENT): [],
      EnumToString.convertToString(OrderType.OPEN): [],
      EnumToString.convertToString(OrderType.COMPLETED): [],
      EnumToString.convertToString(OrderType.CANCELLED): [],
    };
  }

  void setOrder(Order temp, OrderType orderType) {
    final idx = orders[EnumToString.convertToString(orderType)]
        .indexWhere((x) => x.id == temp.id);
    orders[EnumToString.convertToString(orderType)][idx] = temp;
    update();
  }

  void addOrder(Order order, OrderType orderType) {
    orders[EnumToString.convertToString(orderType)].add(order);
    update();
  }

  void setOrders(List<Order> temp, OrderType orderType) {
    orders[EnumToString.convertToString(orderType)] = temp;
    update();
  }
}
