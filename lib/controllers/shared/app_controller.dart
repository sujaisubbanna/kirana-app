import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skipyq_app/controllers/shared/admin_controller.dart';
import 'package:skipyq_app/controllers/shared/customer_controller.dart';
import 'package:skipyq_app/controllers/shared/owner_controller.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';

class AppController extends GetxController {
  final GetStorage storage = GetStorage();

  String authToken;
  String pushToken;

  void init() {
    authToken = storage.read('auth_token');
  }

  void setAuthToken(String token) {
    authToken = token;
    storage.write('auth_token', token);
  }

  void clear() {
    authToken = null;
    storage.remove('auth_token');
    pushToken = null;
  }

  void logout() {
    clear();
    Get.find<UserController>().clear();
    Get.find<AdminController>().clear();
    Get.find<OwnerController>().clear();
    Get.find<CustomerController>().clear();
  }
}
