import 'package:enum_to_string/enum_to_string.dart';
import 'package:skipyq_app/models/owner/owner_model.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';
import 'package:skipyq_app/models/shared/store_tags_enum.dart';
import 'package:skipyq_app/models/store/payment_details_model.dart';
import 'package:skipyq_app/models/store/store_type_enum.dart';

class Store {
  String id;
  String name;
  String contactNo;
  LocationModel location;
  String image;
  dynamic owner;
  DateTime createdAt;
  DateTime updatedAt;
  Map<String, List<String>> orders = {
    EnumToString.convertToString(OrderType.OPEN): [],
    EnumToString.convertToString(OrderType.COMPLETED): [],
    EnumToString.convertToString(OrderType.CANCELLED): [],
  };
  bool open;
  bool active;
  bool onlinePaymentEnabled;
  bool paymentRequired;
  PaymentDetails paymentDetails;
  StoreType type;
  double dist;
  List<StoreTag> tags;

  Store({
    this.id,
    this.name,
    this.contactNo,
    this.createdAt,
    this.updatedAt,
    this.location,
    this.image,
    this.owner,
    this.orders,
    this.active,
    this.open,
    this.onlinePaymentEnabled,
    this.paymentRequired,
    this.paymentDetails,
    this.type,
    this.dist,
    this.tags,
  });

  Store.fromJson(Map<String, dynamic> json) {
    id = json['_id'] as String;
    name = json['name'] as String;
    contactNo = json['contactNo'] as String;
    location = LocationModel.fromJson(json['location']);
    image = json['image'] as String;
    if (json['owner'] is String) {
      owner = json['owner'] as String;
    } else {
      owner = Owner.fromJson(json['owner']);
    }
    dist = json['dist'] ?? null;
    if (dist != null) {
      dist = double.parse((dist / 1000).toStringAsFixed(1));
    }
    OrderType.values.forEach((e) {
      if (e != OrderType.RECENT) {
        orders[EnumToString.convertToString(e)] =
            (json['orders'][EnumToString.convertToString(e)] as List)
                .map((x) => x as String)
                .toList();
      }
    });
    tags = json['tags'] != null
        ? (json['tags'] as List)
            .map((e) => EnumToString.fromString(StoreTag.values, e))
            .toList()
        : [];
    open = json['open'] as bool;
    active = json['active'] as bool;
    onlinePaymentEnabled = json['onlinePaymentEnabled'] as bool;
    paymentRequired = json['paymentRequired'] as bool;
    paymentDetails = json['paymentDetails'] != null
        ? PaymentDetails.fromJson(json['paymentDetails'])
        : null;
    type = EnumToString.fromString(StoreType.values, json['type'] as String);
    createdAt = json['createdAt'] != null
        ? DateTime.parse(json['createdAt'] as String)
        : null;
    updatedAt = json['updatedAt'] != null
        ? DateTime.parse(json['updatedAt'] as String)
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'name': name,
      'contactNo': contactNo,
      'location': location.toJson(),
      'image': image,
      'owner': owner,
      'orders': OrderType.values.map((e) {
        if (e != OrderType.RECENT) {
          return {
            EnumToString.convertToString(e):
                orders[EnumToString.convertToString(e)]
          };
        }
      }).toList(),
      'tags': tags.map((e) => EnumToString.convertToString(e)).toList(),
      'active': active,
      'open': open,
      'onlinePaymentEnabled': onlinePaymentEnabled,
      'paymentRequired': paymentRequired,
      'paymentDetails': paymentDetails.toJson(),
    };
  }
}
