class PaymentDetails {
  VPA vpa;
  BankAccount bankAccount;

  PaymentDetails({
    this.vpa,
    this.bankAccount,
  });

  PaymentDetails.fromJson(Map<String, dynamic> json) {
    vpa = json['vpa'] != null ? VPA.fromJson(json['vpa']) : null;
    bankAccount = json['bankAccount'] != null
        ? BankAccount.fromJson(json['bankAccount'])
        : null;
  }

  Map<String, dynamic> toJson() {
    return {
      'vpa': vpa != null ? vpa.toJson() : null,
      'bankAccount': bankAccount != null ? bankAccount.toJson() : null,
    };
  }
}

class VPA {
  String address;

  VPA({
    this.address,
  });

  VPA.fromJson(Map<String, dynamic> json) {
    address = json['address'] as String;
  }

  Map<String, dynamic> toJson() {
    return {
      'address': address,
    };
  }
}

class BankAccount {
  String ifsc;
  String name;
  String accountNumber;

  BankAccount({
    this.ifsc,
    this.name,
    this.accountNumber,
  });

  BankAccount.fromJson(Map<String, dynamic> json) {
    ifsc = json['ifsc'] as String;
    name = json['name'] as String;
    accountNumber = json['accountNumber'] as String;
  }

  Map<String, dynamic> toJson() {
    return {
      'ifsc': ifsc,
      'name': name,
      'accountNumber': accountNumber,
    };
  }
}
