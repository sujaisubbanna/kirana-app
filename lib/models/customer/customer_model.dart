import 'package:enum_to_string/enum_to_string.dart';
import 'package:skipyq_app/models/shared/identity_model.dart';
import 'package:skipyq_app/models/shared/location_model.dart';
import 'package:skipyq_app/models/shared/order_type.enum.dart';

class Customer {
  String id;
  String name;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic identity;
  LocationModel location;
  Map<String, List<String>> orders = {
    EnumToString.convertToString(OrderType.OPEN): [],
    EnumToString.convertToString(OrderType.COMPLETED): [],
    EnumToString.convertToString(OrderType.CANCELLED): [],
  };

  Customer({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.identity,
    this.location,
    this.orders,
  });

  Customer.fromJson(Map<String, dynamic> json) {
    id = json['_id'] as String;
    name = json['name'] as String;
    if (json['identity'] is String) {
      identity = json['identity'] as String;
    } else {
      identity = Identity.fromJson(json['identity']);
    }
    createdAt = json['createdAt'] != null
        ? DateTime.parse(json['createdAt'] as String)
        : null;
    updatedAt = json['updatedAt'] != null
        ? DateTime.parse(json['updatedAt'] as String)
        : null;
    location = json['location'] != null
        ? LocationModel.fromJson(json['location'])
        : null;
    OrderType.values.forEach((e) {
      if (e != OrderType.RECENT) {
        orders[EnumToString.convertToString(e)] =
            (json['orders'][EnumToString.convertToString(e)] as List)
                .map((x) => x as String)
                .toList();
      }
    });
  }
}
