import 'package:skipyq_app/models/shared/identity_model.dart';

class Owner {
  String id;
  String name;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic identity;
  List<String> stores;

  Owner({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.identity,
    this.stores,
  });

  Owner.fromJson(Map<String, dynamic> json) {
    id = json['_id'] as String;
    name = json['name'] as String;
    if (json['identity'] is String) {
      identity = json['identity'] as String;
    } else {
      identity = Identity.fromJson(json['identity']);
    }
    stores = (json['stores'] as List).map((e) => e.toString()).toList();
    createdAt = json['createdAt'] != null
        ? DateTime.parse(json['createdAt'] as String)
        : null;
    updatedAt = json['updatedAt'] != null
        ? DateTime.parse(json['updatedAt'] as String)
        : null;
  }
}
