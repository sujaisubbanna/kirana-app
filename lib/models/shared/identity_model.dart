import 'package:enum_to_string/enum_to_string.dart';
import 'package:skipyq_app/models/shared/role_enum.dart';

class Identity {
  bool verified;
  String pushToken;
  Roles role;
  String id;
  String mobile;
  DateTime createdAt;
  DateTime updatedAt;

  Identity({
    this.id,
    this.verified,
    this.pushToken,
    this.role,
    this.mobile,
    this.createdAt,
    this.updatedAt,
  });

  Identity.fromJson(Map<String, dynamic> json) {
    id = json['_id'] as String;
    role = EnumToString.fromString(
      Roles.values,
      json['role'] as String,
    );
    verified = json['verified'] as bool;
    pushToken = json['pushToken'] != null ? json['pushToken'] as String : null;
    mobile = json['mobile'] as String;
    createdAt = json['createdAt'] != null
        ? DateTime.parse(json['createdAt'] as String)
        : null;
    updatedAt = json['updatedAt'] != null
        ? DateTime.parse(json['updatedAt'] as String)
        : null;
  }
}
