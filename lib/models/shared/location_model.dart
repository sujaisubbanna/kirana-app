class GeoLocation {
  List<double> coordinates;
  String type;

  GeoLocation({
    this.coordinates,
    this.type,
  });

  GeoLocation.fromJson(Map<String, dynamic> json) {
    coordinates = new List<double>.from(json['coordinates'] as List);
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    return {
      'coordinates': coordinates,
      'type': type,
    };
  }
}

class LocationModel {
  String address;
  GeoLocation geoLocation;

  LocationModel({
    this.address,
    this.geoLocation,
  });

  LocationModel.fromJson(Map<String, dynamic> json) {
    address = json['address'] as String;
    geoLocation = GeoLocation.fromJson(json['geoLocation']);
  }

  Map<String, dynamic> toJson() {
    return {
      'address': address,
      'geoLocation': geoLocation.toJson(),
    };
  }
}
