import 'package:enum_to_string/enum_to_string.dart';
import 'package:skipyq_app/models/customer/customer_model.dart';
import 'package:skipyq_app/models/shared/item_model.dart';
import 'package:skipyq_app/models/shared/order_status_enum.dart';
import 'package:skipyq_app/models/shared/payment_status_enum.dart';
import 'package:skipyq_app/models/store/store_model.dart';

class Order {
  String id;
  List<Item> items;
  int grossAmount;
  int netAmount;
  int discount;
  OrderStatus status;
  PaymentStatus paymentStatus;
  dynamic customer;
  dynamic store;
  DateTime createdAt;
  DateTime updatedAt;
  int orderNo;
  String notes;

  Order({
    this.id,
    this.items,
    this.grossAmount,
    this.netAmount,
    this.discount,
    this.status,
    this.paymentStatus,
    this.customer,
    this.store,
    this.createdAt,
    this.updatedAt,
    this.orderNo,
    this.notes,
  });

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'items': items.map((e) => e.toJson()).toList(),
      'grossAmount': grossAmount,
      'netAmount': netAmount,
      'discount': discount,
      'status': EnumToString.convertToString(status),
      'paymentStatus': EnumToString.convertToString(paymentStatus),
      'customer': customer,
      'store': store,
      'orderNo': orderNo,
      'notes': notes,
    };
  }

  Order.fromJson(Map<String, dynamic> json) {
    id = json['_id'] as String;
    orderNo = json['orderNo'];
    items = (json['items'] as List).map((e) => Item.fromJson(e)).toList() ?? [];
    grossAmount = json['grossAmount'];
    netAmount = json['netAmount'];
    discount = json['discount'];
    notes = json['notes'];
    status = EnumToString.fromString(OrderStatus.values, json['status']);
    paymentStatus =
        EnumToString.fromString(PaymentStatus.values, json['paymentStatus']);
    customer = json['customer'] is String
        ? json['customer']
        : Customer.fromJson(json['customer']);
    store =
        json['store'] is String ? json['store'] : Store.fromJson(json['store']);
    createdAt = json['createdAt'] != null
        ? DateTime.parse(json['createdAt'] as String)
        : null;
    updatedAt = json['updatedAt'] != null
        ? DateTime.parse(json['updatedAt'] as String)
        : null;
  }
}
