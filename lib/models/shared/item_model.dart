class Item {
  String name;
  String qty;
  double price;

  Item({this.name, this.qty, this.price});

  Item.fromJson(Map<String, dynamic> json) {
    name = json['name'] as String;
    qty = json['qty'];
    price = json['price'].toDouble();
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'qty': qty,
      'price': price,
    };
  }
}
