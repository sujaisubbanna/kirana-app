import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' as service;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skipyq_app/controllers/shared/admin_controller.dart';
import 'package:skipyq_app/controllers/shared/app_controller.dart';
import 'package:skipyq_app/controllers/shared/customer_controller.dart';
import 'package:skipyq_app/controllers/shared/user_controller.dart';
import 'package:skipyq_app/routing/router.dart';
import 'package:skipyq_app/screens/auth/splash_screen.dart';
import 'package:skipyq_app/services/utils/notification_service.dart';
import 'config/firebase_notification.dart';
import 'config/theme.dart';
import 'controllers/shared/owner_controller.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  NotificationService().showNotification(
    message.notification.title,
    message.notification.body,
    message.data.toString(),
  );
}

void main() async {
  await dotenv.load();
  await GetStorage.init();

  WidgetsFlutterBinding.ensureInitialized();
  await NotificationService().init();

  await service.SystemChrome.setPreferredOrientations([
    service.DeviceOrientation.portraitUp,
    service.DeviceOrientation.portraitDown
  ]);

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  void initState() {
    Get.put(UserController());
    Get.put(CustomerController());
    Get.put(AppController()).init();
    Get.put(OwnerController());
    Get.put(AdminController());
    FirebaseNotifications().setUpFirebase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'SkipyQ',
      debugShowCheckedModeBanner: false,
      navigatorObservers: [
        FirebaseAnalyticsObserver(
          analytics: analytics,
        ),
      ],
      theme: theme,
      locale: const Locale('en', 'IN'),
      getPages: AppScreens.screens,
      home: SplashScreen(),
    );
  }
}
