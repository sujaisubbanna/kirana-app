import 'package:get/get.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/screens/customer/customer_create_order_screen.dart';
import 'package:skipyq_app/screens/customer/customer_edit_profile_screen.dart';
import 'package:skipyq_app/screens/customer/customer_home_screen.dart';
import 'package:skipyq_app/screens/customer/customer_order_screen.dart';
import 'package:skipyq_app/screens/customer/customer_orders_screen.dart';
import 'package:skipyq_app/screens/customer/customer_signup_screen.dart';
import 'package:skipyq_app/screens/customer/customer_store_payment_screen.dart';
import 'package:skipyq_app/screens/owner/store_items_screen.dart';
import 'package:skipyq_app/screens/owner/store_order_screen.dart';
import 'package:skipyq_app/screens/shared/settings_screen.dart';

List<GetPage> getCustomerRoutes() {
  final List<GetPage> screens = [
    GetPage(
      name: routes.HOME,
      page: () => CustomerHomeScreen(),
    ),
    GetPage(
      name: routes.CUSTOMER_SIGN_UP,
      page: () => CustomerSignUpScreen(),
    ),
    GetPage(
      name: routes.CREATE_ORDER,
      page: () => CustomerCreateOrderScreen(),
    ),
    GetPage(
      name: routes.CUSTOMER_ORDER,
      page: () => CustomerOrderScreen(),
    ),
    GetPage(
      name: routes.CUSTOMER_ORDERS,
      page: () => CustomerOrdersScreen(),
    ),
    GetPage(
      name: routes.SETTINGS,
      page: () => SettingsScreen(),
    ),
    GetPage(
      name: routes.STORE_ORDER,
      page: () => StoreOrderScreen(),
    ),
    GetPage(
      name: routes.ORDER_ITEMS,
      page: () => OrderItemsScreen(),
    ),
    GetPage(
      name: routes.CUSTOMER_PAYMENT_DETAILS,
      page: () => CustomerStorePaymentScreen(),
    ),
    GetPage(
      name: routes.CUSTOMER_EDIT,
      page: () => CustomerEditScreen(),
    ),
  ];
  return screens;
}
