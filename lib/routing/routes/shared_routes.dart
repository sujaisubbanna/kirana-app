import 'package:get/get.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/screens/shared/feedback_screen.dart';

List<GetPage> getSharedRoutes() {
  final List<GetPage> screens = [
    GetPage(
      name: routes.FEEDBACK,
      page: () => FeedbackScreen(),
    ),
  ];
  return screens;
}
