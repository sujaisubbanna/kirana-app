import 'package:get/get.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/screens/auth/login_screen.dart';
import 'package:skipyq_app/screens/auth/otp_screen.dart';
import 'package:skipyq_app/screens/auth/owner_sign_up_screen.dart';
import 'package:skipyq_app/screens/auth/role_screen.dart';
import 'package:skipyq_app/screens/auth/splash_screen.dart';

List<GetPage> getAuthRoutes() {
  final List<GetPage> screens = [
    GetPage(
      name: routes.ROLE_SELECT,
      page: () => RoleScreen(),
    ),
    GetPage(
      name: routes.OWNER_SIGN_UP,
      page: () => OwnerSignUpScreen(),
    ),
    GetPage(
      name: routes.SPLASH,
      page: () => SplashScreen(),
    ),
    GetPage(
      name: routes.LOGIN,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: routes.OTP,
      page: () => OTPScreen(),
    ),
  ];
  return screens;
}
