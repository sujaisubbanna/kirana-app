import 'package:get/get.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/screens/owner/add_bank_account_screen.dart';
import 'package:skipyq_app/screens/owner/add_store_screen.dart';
import 'package:skipyq_app/screens/owner/add_upi_screen.dart';
import 'package:skipyq_app/screens/owner/edit_store_screen.dart';
import 'package:skipyq_app/screens/owner/manage_store_screen.dart';
import 'package:skipyq_app/screens/owner/payment_details_screen.dart';
import 'package:skipyq_app/screens/owner/store_orders_screen.dart';
import 'package:skipyq_app/screens/owner/stores_screen.dart';

List<GetPage> getOwnerRoutes() {
  final List<GetPage> screens = [
    GetPage(
      name: routes.STORES,
      page: () => StoresScreen(),
    ),
    GetPage(
      name: routes.ADD_STORE,
      page: () => AddStoreScreen(),
    ),
    GetPage(
      name: routes.MANAGE_STORE,
      page: () => ManageStoreScreen(),
    ),
    GetPage(
      name: routes.STORE_ORDERS,
      page: () => StoreOrdersScreen(),
    ),
    GetPage(
      name: routes.EDIT_STORE,
      page: () => EditStoreScreen(),
    ),
    GetPage(
      name: routes.ADD_BANK_ACCOUNT,
      page: () => BankDetailsScreen(),
    ),
    GetPage(
      name: routes.ADD_UPI_ACCOUNT,
      page: () => UPIDetailsScreen(),
    ),
    GetPage(
      name: routes.STORE_PAYMENT_DETAILS,
      page: () => StorePaymentDetailsScreen(),
    ),
  ];
  return screens;
}
