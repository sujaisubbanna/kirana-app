import 'package:get/get.dart';
import 'package:skipyq_app/routing/routes.dart' as routes;
import 'package:skipyq_app/screens/admin/add_owner_screen.dart';
import 'package:skipyq_app/screens/admin/dashboard_screen.dart';
import 'package:skipyq_app/screens/admin/owners_screen.dart';

List<GetPage> getAdminRoutes() {
  final List<GetPage> screens = [
    GetPage(
      name: routes.DASHBOARD,
      page: () => DashboardScreen(),
    ),
    GetPage(
      name: routes.ADD_OWNER,
      page: () => AddOwnerScreen(),
    ),
    GetPage(
      name: routes.OWNERS,
      page: () => OwnersScreen(),
    ),
  ];
  return screens;
}
