import 'package:skipyq_app/routing/routes/admin_routes.dart';
import 'package:skipyq_app/routing/routes/auth_routes.dart';
import 'package:skipyq_app/routing/routes/customer_routes.dart';
import 'package:skipyq_app/routing/routes/owner_routes.dart';
import 'package:skipyq_app/routing/routes/shared_routes.dart';

abstract class AppScreens {
  static final screens = [
    ...getAuthRoutes(),
    ...getAdminRoutes(),
    ...getOwnerRoutes(),
    ...getCustomerRoutes(),
    ...getSharedRoutes(),
  ];
}
